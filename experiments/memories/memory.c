/*
 * Copyright (c) 2018 Francis Laniel <francis.laniel@lip6.fr>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distribute*d with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <strings.h>
#include <sys/sysinfo.h>
#include <limits.h>
#include <signal.h>
#include <time.h>

#define RANDOM 0
#define SEQUENTIAL 1

/**
 * There are 60 seconds in a minutes, so there are 600 seconds in 10 minutes.
 *
 * This define is used to drag our experiments for 10 minutes.
 */
#define TIME 600

/**
 * Our experiments are basically about manipulate a buffer during a given at
 * compilation time.
 *
 * Since we allocated memory for the buffer we need to free it.
 * This variable will be set to false by SIGALRM after the time so the
 * "infinite" loop will break and the execution will continue and free the
 * buffer.
 */
char ok = 1;

/**
 * Return the total physical memory size in bytes.
 *
 * It basically multiplies the number of physical page by the size of a physical
 * page.
 *
 * @return The total physical memory size in bytes.
 */
unsigned long long get_memory_size_in_bytes(){
	long int physical_pages;

	if((physical_pages = get_phys_pages()) == -1){
		perror("get_phys_pages");

		exit(EXIT_FAILURE);
	}

	return physical_pages * getpagesize();
}

/**
 * Flush a line which contains the given as argument address from the cache.
 *
 * @param addr The address to flush from the cache.
 */
void clflush(void *addr){
	asm volatile("clflush (%0)" :: "r"(addr));
}

/**
 * This function executed when SIGALRM is received will pass the global variable
 * ok to 0 so the loop in random_memory and sequential_memory will break.
 *
 * @param unused I think that the name is clear.
 */
void handle_SIGALRM(int unused){
	ok = 0;
}

/**
 * Generate a long long random number.
 *
 * WARNING You should have called srand(seed) before calling this function.
 *
 * @return A long long random number.
 */
unsigned long long big_random(){
	unsigned i;

	unsigned long long ret;

	ret = 0;

	for(i = 0; i < sizeof(long long) / sizeof(int); i++)
		/*
		 * At least this function will return an int random number due to this if
		 * because at the first iteration i will be 0 so we will jump to the else.
		 *
		 * For other i we will toss a coin, if it is head then we regenerate an int
		 * random number that will be binary ored and shifted to be "concatenated"
		 * to the already generated number to get a bigger random number.
		 *
		 * I would like to thank a colleague for an optimization in the if compared
		 * to the previous version of this function I wrote.
		 */
		if(rand() % 2 || i == 0)
			ret |= ((unsigned long long) rand()) << i * sizeof(int) * CHAR_BIT;

	return ret;
}

/**
 * Access randomly a buffer.
 *
 * This function will execute during TIME until SIGALRM is received.
 *
 * @param memory The buffer which will be randomly accessed.
 * @param size The size of the buffer.
 */
void random_memory(char *memory, unsigned long long size){
	srand(time(NULL));

	while(ok){
		unsigned long long rand;

		char val;

		rand = big_random() % size;

		val = memory[rand];

		memory[rand] = val;

		clflush(&memory[rand]);
	}
}

/**
 * Access sequentially each cache line which compose a buffer.
 *
 * This function will execute during TIME until SIGALRM is received.
 *
 * @param memory The buffer which will be sequentially accessed.
 * @param size The size of the buffer.
 */
void sequential_memory(char *memory, unsigned long long size){
	unsigned long long i;

	int cache_line_size;

	char val;

	if((cache_line_size = sysconf(_SC_LEVEL1_DCACHE_LINESIZE)) == -1){
		perror("sysconf");

		return;
	}

	while(ok){
		/**
		 * Loop through each cache line which compose the whole memory.
		 *
		 * The first byte in the cache line will be read then written and the cache
		 * line will be evicted.
		 */
		for(i = 0; i < size; i += cache_line_size){
			val = memory[i];

			memory[i] = val;

			clflush(&memory[i]);
		}
	}
}

/**
 * Interact sequentially or randomly with a memory buffer.
 *
 * This function will execute during TIME until SIGALRM is received if the
 * caller provided accepted value for interaction_type.
 *
 * @param memory The buffer which will be sequentially or randomly accessed.
 * @param interaction_type Indicates how the memory will be accessed: either
 * SEQUENTIALly or RANDOMly.
 * @param size The size of the memory.
 */
void interact_with_memory(char *memory, char interaction_type,
													unsigned long long size){
	switch(interaction_type){
		case RANDOM:
			random_memory(memory, size);

			break;
		case SEQUENTIAL:
			sequential_memory(memory, size);

			break;
		default:
			fprintf(stderr, "Bad value for interaction_type\n");
	}
}

/**
 * This program allocates big (possibly 80% of total memory) and manipulates it
 * during ten minutes either sequentially or randomly.
 *
 * It can takes two command line arguments:
 * * -r to indicate to manipulate the memory randomly (the default behavior is
 * sequentially).
 * * -s size to indicate the size of the buffer in bytes (default to 80% of
 * total memory.)
 *
 * @param argc The number of arguments provided.
 * @param argv The command line arguments.
 * @return EXIT_SUCCESS if the program finished or EXIT_FAILURE if a call to a
 * POSIX function failed.
 */
int main(int argc, char **argv){
	char opt;

	/*
	 * The size will be given in GB so when we use it for "malloc" or as a for
	 * condition we will need to left shift it by 30 to get the size in bytes.
	 */
	unsigned long long size;
	char interaction_type;

	char *memory;

	struct sigaction sig_action;

	timer_t timer;
	struct itimerspec timer_spec;

	/*
	 * If the user does not provide a memory size we default to 80% of the total
	 * memory.
	 * I choose the value of 80% to avoid swap and/or out of memory killer.
	 */
	size = get_memory_size_in_bytes() * .8;

	interaction_type = SEQUENTIAL;

	// Arguments parsing.
	while((opt = getopt(argc, argv, "rs:")) != -1){
		switch(opt){
			case 'r':
				interaction_type = RANDOM;

				break;
			case 's':
				/*
				 * As indicate in the man page for strtoul we need to put errno at 0
				 * and then check its values after the call of the function to see if
				 * it failed or no.
				 */
				errno = 0;

				// Convert the argument of the -s option to an unsigned long long.
				size = strtoull(optarg, NULL, 10);

				if(errno){
					perror("strtoul");

					return EXIT_FAILURE;
				}

				break;
			default:
				fprintf(stderr, "Usage: %s [-r] [-s size_in_bytes]\n", argv[0]);

				return EXIT_FAILURE;
		}
	}

	printf("size: %lluB\n", size);

	/*
	 * Allocate size << 30 bytes aligned on a page size.
	 *
	 * We need to cast &memory in void ** because posix_memalign awaits this type
	 * as first argument.
	 */
	if(posix_memalign((void **) &memory, getpagesize(), size)){
		perror("posix_memalign");

		return EXIT_FAILURE;
	}

	/*
	 * We need to manipulate the memory once to get it really allocated since
	 * Linux does "lazy allocation" (i.e. allocate a page only when it is really
	 * needed).
	 * So we write the whole with 0s.
	 */
	bzero(memory, size);

	if(timer_create(CLOCK_MONOTONIC, NULL, &timer) == -1){
		perror("timer_create");

		goto end;
	}

	/**
	 * The POSIX structure have a lot of fields.
	 *
	 * Those fields are read by POSIX function and it is not possible to let
	 * garbage value in those fields this values can be interpreted... badly!
	 *
	 * So the solution is to fill the struct with 0.
	 */
	bzero(&sig_action, sizeof sig_action);

	sig_action.sa_handler = handle_SIGALRM;

	if(sigaction(SIGALRM, &sig_action, NULL)){
		perror("sigaction");

		goto end;
	}

	bzero(&timer_spec, sizeof timer_spec);

	timer_spec.it_value.tv_sec = TIME;

	// Arm the timer so it expires in 10 minutes and fires SIGALRM.
	if(timer_settime(timer, 0, &timer_spec, NULL) == -1){
		perror("timer_settime");

		goto end;
	}

	interact_with_memory(memory, interaction_type, size);

end:
	free(memory);

	return EXIT_SUCCESS;
}