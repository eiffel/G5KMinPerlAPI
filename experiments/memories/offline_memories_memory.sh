#! /usr/bin/env bash
# Copyright 2017 Francis Laniel <francis.laniel@lip6.fr>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Test if run as root.
if [ $EUID -ne 0 ]; then
	echo "This script must be run as root." >&2

	exit
fi

if [ $# -lt 1 ]; then
	echo "Usage : $0 offline_bool [memory_binary_options]" >&2

	exit
fi

offline_bool=$1
memory_binary_options=$2

# The sysfs is a complicated tree with multiple symbolic link which can point
# to the same place.
# So we just get the last result with the tail command.
intel_rapl_root=$(find /sys -name "intel-rapl" | tail -1)

# Test the length of the variable intel_rapl_root.
if [ -z $intel_rapl_root ]; then
	echo "No directory for RAPL package was found!" >&2

	exit
fi

dram_energy_files=""
package_energy_files=""

# For each RAPL domain we try to find the one that corresponds to DRAM and CPU.
for i in $(find $intel_rapl_root/ -name "name"); do
	if [ $(cat $i) == "dram" ]; then
		# If we found the package we can get the file which contains energy
		# consumption by replacing name by energy_uj.
		# On some computer (e.g. > 1 NUMA node computer) it is possible to have
		# multiple file to get DRAM energy.
		# In this case we add the current name to a string which contains all the
		# file to get DRAM energy.
		dram_energy_files="$(echo $i | sed 's/name/energy_uj/') ${dram_energy_files}"
	fi

	if [[ $(cat $i) =~ package.* ]]; then
		package_energy_files="$(echo $i | sed 's/name/energy_uj/') ${package_energy_files}"
	fi
done

if [ -z "$dram_energy_files" ]; then
	echo "No file for dram energy consumption was found!" >&2

	exit
fi

if [ -z "$package_energy_files" ]; then
	echo "No file for package energy consumption was found!" >&2

	exit
fi

if [ $offline_bool -ne 0 ]; then
	# Put the maximum memory offline !
	for i in /sys/devices/system/memory/memory*; do
		if [ $(cat $i/removable) -eq 1 ]; then
			echo offline > $i/state || (echo -e "Problem while offlining $i.\n" >&2)
		fi
	done
fi

dram_energy_before=0
package_energy_before=0

# Get the energy consumed before the sleep.
for i in $dram_energy_files; do
	dram_energy_before=$((dram_energy_before + $(cat $i)))
done

for i in $package_energy_files; do
	package_energy_before=$((package_energy_before + $(cat $i)))
done

if [ ! -f memory_exp ]; then
	echo "Binary memory not found!" >&2

	exit
fi

# The binary memory can take some options.
# The option to transfer to this binary as second argument and enclosed between
# quotes.
./memory_exp $memory_binary_options

dram_energy_after=0
package_energy_after=0

# Get the energy consumed after the sleep.
for i in $dram_energy_files; do
	dram_energy_after=$((dram_energy_after + $(cat $i)))
done

for i in $package_energy_files; do
	package_energy_after=$((package_energy_after + $(cat $i)))
done

# Get the energy consumed during the sleep by subtracting the energy consumed
# before the sleep to the actual consumed energy.
dram_energy=$((dram_energy_after - dram_energy_before))
package_energy=$((package_energy_after - package_energy_before))

echo "Energy consumption is ${dram_energy} µJ for DRAM and ${package_energy} µJ for SOC with $(cat /proc/meminfo | head -1 | awk '{print $2 $3}') for 10m."