#! /usr/bin/env perl
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
use strict;
use warnings;
use lib q{../lib};
use Carp;
use Readonly;
use Scalar::Util qw(reftype);
use G5KMinPerlAPI::Experiments qw(
	get_power_and_deploy_capable_available_nodes_sold_by_the_devil
	reserve_deploy_and_connect_job launch_script_and_get_power);
use G5KMinPerlAPI::SSHez qw(make_site_to_node_tunnel put_file_in_remote_home);
use Term::ReadKey;
use Term::ReadLine;
use threads;

# We will run 4 experiments:
# 0. Read sequentially 80% of the whole memory.
# 1. Put offline as memory as possible and read sequentially the 80% of the new
# whole memory.
# 2. Read sequentially the same amount of memory available in 1.
# 3. Read randomly the same amount of memory available in 1.
Readonly my $SCRIPT_OPTIONS => ["0", "1", "0 '-s %llu'", "0 '-r -s %llu'"];

sub exp1{
	my $arg_ref;

	my $username;
	my $password;
	my $ssh;
	my $site;
	my $node;
	my $script;
	my $script_options;
	my $binary;

	my $before_command;

	my $out;
	my $err;

	my $fd_out;
	my $fd_err;

	my $fd_in;
	my $line;

	my $filename_out;
	my $filename_err;

	my $json;
	my $proxy_cmd;

	my $mem_size;

	my $i;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$ssh = $arg_ref->{ssh} or croak 'Field ssh is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$node = $arg_ref->{node} or croak 'Field node is empty';
	$script = $arg_ref->{script} or croak 'Field script is empty';
	$script_options = $arg_ref->{script_options}
		or croak 'Field script_options is empty';
	$binary = $arg_ref->{binary} or croak 'Field binary is empty';

	if(not put_file_in_remote_home($ssh->{ssh_node}, $script)){
		warn "Impossible to upload $script on $node:$site!";

		return 0;
	}

	if(not put_file_in_remote_home($ssh->{ssh_node}, $binary)){
		warn "Impossible to upload $binary on $node:$site!";

		return 0;
	}

	$i = 0;

	foreach my $it (@{$script_options}){
		$filename_out = "$node-$site.exp$i";
		$filename_err = "$node-$site.exp$i.err";

		open $fd_out, ">", $filename_out or die "Can not open $filename_out: $!";
		open $fd_err, ">", $filename_err or die "Can not open $filename_err: $!";

		if($i > 1){
			# For experiment 2. and 3. explained above we need to indicate a memory
			# size.
			# The memory size was provided at the end of experiment 1. and this used
			# now as an argument to the script.
			$it = sprintf $it, $mem_size;
		}

		launch_script_and_get_power({username => $username, password => $password,
		site => $site, node => $node, ssh => $ssh->{ssh_node},
		command => "./$script $it", fd_out => $fd_out, fd_err => $fd_err});

		$ssh->{ssh_node}->system('reboot');

		if($i == 1){
			# Experiment 1. put offline some memory, we need to save the value of the
			# avaiable memory after offlining.
			#
			# Happily this amount of memory is given in kB in the last echo of the
			# script.
			# So we just need to parse $filename_out.
			open $fd_in, "<", $filename_out or die "Can not open $filename_out: $!";

			do{
				$line = <$fd_in>;
			}while($line !~ /size: (\d+)B/);

			$mem_size = $1;

			close $fd_in or warn "Can not close $filename_out: $!";
		}

		# We need to sleep to wait the end of the reboot.
		sleep 480;

		$ssh->{ssh_node} = make_site_to_node_tunnel($ssh->{ssh_site}, $site, $node);

		$i++;

		close $fd_out or warn "Can not close $filename_out: $!";
		close $fd_err or warn "Can not close $filename_err: $!";
	}

}

sub exp1_thread{
	my $arg_ref;

	my $username;
	my $password;
	my $site;
	my $node;
	my $time;
	my $ssh_public_key;
	my $script;
	my $script_options;
	my $binary;

	my $ssh;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$node = $arg_ref->{node} or croak 'Field node is empty';
	$time = $arg_ref->{'time'} or croak 'Field time is empty';
	$ssh_public_key = $arg_ref->{ssh_public_key}
		or croak 'Field ssh_public_key is empty';
	$script = $arg_ref->{script} or croak 'Field script is empty';
	$script_options = $arg_ref->{script_options}
		or croak 'Field script_options is empty';
	$binary = $arg_ref->{binary} or croak 'Field binary is empty';


	$ssh = reserve_deploy_and_connect_job({username => $username,
		password => $password, site => $site, node => $node, 'time' => $time,
		ssh_key => $ssh_public_key});

	exp1({username => $username, password => $password, ssh => $ssh,
		site => $site, node => $node, script => $script,
		script_options => $script_options, binary => $binary});
}

my $username;
my $password;

my $time;
my $ssh_key_pathname;
my $script;
my $binary;

my $hash;

my $fd;
my $ssh_key;

my $thread;
my @threads;

$username = shift @ARGV
	or croak "Usage : $0 Grid'5000_username walltime ssh_public_key script_to_execute binary_to_launch";
$time = shift @ARGV
	or croak "Usage : $0 Grid'5000_username walltime ssh_public_key script_to_execute binary_to_launch";
$ssh_key_pathname = shift @ARGV
	or croak "Usage : $0 Grid'5000_username walltime ssh_public_key script_to_execute binary_to_launch";
$script = shift @ARGV
	or croak "Usage : $0 Grid'5000_username walltime ssh_public_key script_to_execute binary_to_launch";
$binary = shift @ARGV
	or croak "Usage : $0 Grid'5000_username walltime ssh_public_key script_to_execute binary_to_launch";

print "Enter your password:\n";

ReadMode('noecho');
$password = ReadLine(0);
ReadMode('restore');

chomp $password;

# Read the public key contained in the pathname given as argument.
open $fd, '<', $ssh_key_pathname or die "Can not open < $ssh_key_pathname: $!";
$ssh_key = <$fd>;
close $fd or warn "Can not close $fd: $!";

if(not length $ssh_key){
	croak 'ssh key is empty!';
}

$hash = get_power_and_deploy_capable_available_nodes_sold_by_the_devil(
	$username, $password, $time);

foreach my $it (keys %{$hash}){
	print "$it:\n";

	foreach my $jt (@{$hash->{$it}}){
		print "\t* $jt\n";

# 		exp1_thread({username => $username, password => $password, site => $it,
# 			node => $jt, 'time' => $time, ssh_public_key => $ssh_key,
# 			script => $script, script_options => $SCRIPT_OPTIONS, binary => $binary});

		$thread = threads->create(\&exp1_thread, {username => $username,
			password => $password, site => $it, node => $jt, 'time' => $time,
			ssh_public_key => $ssh_key, script => $script, binary => $binary,
			script_options => $SCRIPT_OPTIONS});

		push @threads, $thread;
	}
}

foreach my $it (@threads){
	$it->join();
}