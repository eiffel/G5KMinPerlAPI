#! /usr/bin/env perl
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
use strict;
use warnings;
use Scalar::Util qw(reftype);
use Readonly;
use Carp;
use Test::More;

BEGIN{
	plan tests => 19;
	use_ok('G5KMinPerlAPI::Common', qw(get_sites get_clusters get_nodes
		get_job_characteristics get_deploy_characteristics get_site_status
		get_node_characteristics get_node_power_consumption));
	use_ok('G5KMinPerlAPI::Capable', qw(is_running is_deployed is_deploy_capable
		is_available_and_not_reserved is_API_and_not_shared_power_capable
		is_sold_by_the_devil));
	use_ok('G5KMinPerlAPI::Jobs', qw(reserve_deploy_node delete_job do_deploy));
	use_ok('G5KMinPerlAPI::SSHez', qw(make_ssh_tunnel put_file_in_remote_home
		get_file_in_local_wd));
	use_ok('G5KMinPerlAPI::Experiments', qw(to_seconds));
}

# If the reservation and deployment succedeed we will put the script dummy.sh
# to the deployed environment. We will then execute it, this script write foo
# in a file called bar.
# After the execution we get back bar and inspect its content, it must contain
# foo.
Readonly my $SCRIPT => 'dummy.sh';
Readonly my $BAR => 'bar';
Readonly my $FOO => 'foo';
Readonly my $COMMAND => "#! /usr/bin/env bash\necho $FOO > $BAR";
Readonly my $PERM => 0755;

# This command will be launched on the remote node and its power consumption
# will be gathered.
# Normally its power consumption must be a square curve (big consumption during
# stress, low during sleep).
Readonly my $STRESS_COMMAND =>
	'for i in {1..10}; do stress -c 4 --timeout 10s; sleep 10; done';

# JSON field which contains the timestamp and the values of power consumption.
Readonly my $ITEMS => 'items';

# JSON field which contains the timestamp where power consumption was measured.
Readonly my $TIMESTAMPS => 'timestamps';

# JSON field which contains the name of the node.
Readonly my $UID => 'uid';

# JSON field which contains the power measured over the time.
Readonly my $VALUES => 'values';

# The gate of Grid'5000 is access.grid5000.fr.
# The 'grid5000.fr' string will be appended in Jobs::reserve_deploy_node.
Readonly my $GATEWAY => 'access';

# Remixed version of
# Experiments::get_power_and_deploy_capable_available_nodes_sold_by_the_devil
#
# This subroutine return a pair {site, node} whom node is power and deploy
# capable, available for the given time and sold by the devil...
#
# This subroutine is not cool because it is a big copy/paste but it permits to
# automate this test because it will get a {site, node} by itself.
sub get_power_and_deploy_capable_available_node_sold_by_the_devil{
	my $username;
	my $password;
	my $job_time;

	my $sites;
	my $clusters;
	my $nodes;

	my $json_site;
	my $json_node;

	$username = shift @_ or croak 'Must provide 3 arguments (first is a string)';
	$password = shift @_ or croak 'Must provide 3 arguments (second is a string)';
	$job_time = shift @_
		or croak 'Must provide 3 arguments (third is an integer)';

	$sites = get_sites({username => $username, password => $password});

	if(reftype $sites ne reftype []){
		die 'get_sites return an array reference';
	}

	# Translate a classical job duration (HH[:MM[:SS]]) in second.
	$job_time = to_seconds($job_time);

	foreach my $it (@{$sites}){
		$clusters = get_clusters({username => $username, password => $password,
			site => $it});

		if(reftype $clusters ne reftype []){
			die 'get_clusters return an array reference';
		}

		$json_site = get_site_status({username => $username, password => $password,
			site => $it});

		foreach my $jt (@{$clusters}){
			$nodes = get_nodes({username => $username, password => $password,
			site => $it, cluster => $jt});

			if(reftype $nodes ne reftype []){
				die 'get_nodes return an array reference';
			}

			foreach my $kt (@{$nodes}){
				$json_node = get_node_characteristics({username => $username,
					password => $password, site => $it, cluster => $jt, node => $kt});

				if(is_API_and_not_shared_power_capable($json_node)
					and is_available_and_not_reserved({json => $json_site, site => $it,
						node => $kt, job_time => $job_time})
					and is_sold_by_the_devil($json_node)
					and is_deploy_capable($json_node)){
					return {$it, $kt};
				}
			}
		}
	}

	return {};
}

my $username;
my $password;
my $ssh_key;

my $gateway;
my $site;
my $node;

my $hash;
my $json;

my $job_id;

my $ssh_hash;

my $fd;
my $line;

my $before_command;

my $timestamps;
my $values;

# Test must be automated, so instead of prompting for credentials we hide them
# in environment variables.
# So before launching this test tester must run:
# $ export G5K_USERNAME=username
# $ read -s G5K_PASSWORD
# $ export G5K_PASSWORD
# $ export G5K_PASSWORD=$(cat ~/.ssh/id_rsa.pub)
# WARNING Be sure to use id_rsa.pub and not id_rsa
$username = $ENV{'G5K_USERNAME'};
$password = $ENV{'G5K_PASSWORD'};
$ssh_key = $ENV{'G5K_PUBLIC_KEY'};

$hash = get_power_and_deploy_capable_available_node_sold_by_the_devil($username,
	$password, '00:30');

is(reftype $hash, reftype {},
	'get_power_and_deploy_capable_available_node_sold_by_the_devil return type');
ok(keys %{$hash}, 'Returned hash is not empty');

# keys return an array so to access the first key we just get the first call of
# the returned array.
$site = (keys %{$hash})[0];
$node = $hash->{$site};

# CSV file which will contain power information.
Readonly my $CSV => "$node.csv";

$json = reserve_deploy_node({username => $username, password => $password,
	site => $site, node => $node, ressources => 'walltime=00:30:00'});

is(reftype $json, reftype {}, 'reserve_deploy_node return type');

$job_id = $json->{$UID};

sleep 30;

$json = do_deploy({username => $username, password => $password,
	site => $site, node => $node, environment => 'debian9-x64-min',
	ssh_key => $ssh_key});

is(reftype $json, reftype {}, 'do_deploy return type');

sleep 300;

$ssh_hash = make_ssh_tunnel({username => $username, gateway => $GATEWAY,
	site => $site, node => $node});

is(reftype $json, reftype {}, 'make_ssh_tunnel return type');

# Create a dummy script which will echo foo in a file called bar.
open $fd, ">", $SCRIPT or die "Can not open $SCRIPT: $!";

print {$fd} $COMMAND or die "Can not write $COMMAND";

# Set it executable.
chmod $PERM, $SCRIPT or die "Can not change permissions of $SCRIPT";

close $fd or warn "Can not close: $!";

ok(put_file_in_remote_home($ssh_hash->{ssh_node}, $SCRIPT),
	'Put the script on remote node');

# Execute the script we sent.
$ssh_hash->{ssh_node}->system("./$SCRIPT")
	or die 'Remote command failed: ' . $ssh_hash->{ssh_node}->error();

# If the script executed it creates a file called bar, we want to get it back.
ok(get_file_in_local_wd($ssh_hash->{ssh_node}, $BAR),
	'Get the distant created file');

open $fd, "<", $BAR or die "Can not open $BAR: $!";

# If the script executed smoothly bar must contain foo.
like(<$fd>, qr/$FOO/, 'bar must contain foo');

close $fd or warn "Can not close: $!";

# We remove the file we got.
unlink $BAR or die "Can not unlink $BAR: $!";

# And we remove the script we created.
unlink $SCRIPT or die "Can not unlink $SCRIPT: $!";

$ssh_hash->{ssh_node}->system('apt install -y -q stress')
	or die 'Remote command failed: ' . $ssh_hash->{ssh_node}->error();

# Get the actual time in second.
$before_command = time;

# With this command we will normaly have a squared curve since power will be
# high during stress and low during sleep.
$ssh_hash->{ssh_node}->system($STRESS_COMMAND)
	or die 'Remote command failed: ' . $ssh_hash->{ssh_node}->error();

# Get the power consumption of the previous command.
$json = get_node_power_consumption({username => $username,
	password => $password, site => $site, node => $node, from => $before_command,
	to => time});

delete_job({username => $username, password => $password, site => $site,
	job_id => $job_id});

is(reftype $json, reftype {}, 'get_node_power_consumption return type');

# Get the first item.
$json = $json->{$ITEMS}[0];

is($json->{$UID}, $node, "Node must be $node and it is $json->{$UID}");

$timestamps = $json->{$TIMESTAMPS};
$values = $json->{$VALUES};

is(reftype $timestamps, reftype [],
	'timestamps returned by get_node_power_consumption must be an array reference');
is(reftype $values, reftype [],
	'values returned by get_node_power_consumption must be an array reference');

ok(@{$timestamps}, 'timestamps must no be empty');
is(@{$timestamps}, @{$values}, 'timestamps and values sizes must be equal');

open $fd, ">", $CSV or die "Can not open $CSV: $!";

# Print for each timestamp the associated value.
# We obtain a CSV file, the plot of this CSV and the form of the curve will
# validate or not our subroutine.
for(my $i = 0; $i < scalar @{$values}; $i++){
	print {$fd} $timestamps->[$i] . ", " . $values->[$i] . "\n";
}

close $fd or warn "Can not close: $!";