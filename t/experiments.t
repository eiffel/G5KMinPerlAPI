#! /usr/bin/env perl
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
use strict;
use warnings;
use Test::More;
use Readonly;
use Carp;
use Scalar::Util qw(reftype);
use threads;

BEGIN{
	use_ok('G5KMinPerlAPI::Experiments', qw(
		get_power_and_deploy_capable_available_nodes_sold_by_the_devil
		reserve_deploy_and_connect_job));
	use_ok('G5KMinPerlAPI::Jobs', qw(delete_job));
}

sub test_thread{
	my $username;
	my $password;
	my $site;

	my $hash;

	$hash = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $hash or reftype $hash ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $hash->{username} or croak 'Field username is empty';
	$password = $hash->{password} or croak 'Field password is empty';
	$site = $hash->{site} or croak 'Field site is empty';

	$hash = reserve_deploy_and_connect_job($hash);

	is(reftype $hash, reftype {}, 'reserve_deploy_and_connect_job return type');

	$hash->{ssh_node}->system('ls')
		or die 'Remote command failed: ' . $hash->{ssh_node}->error();

	delete_job({username => $username, password => $password, site => $site,
		job_id => $hash->{job_id}});
}

my $username;
my $password;
my $ssh_key;

my $verbose;

my $hash;

my $thread;
my @threads;

# Test must be automated, so instead of prompting for credentials we hide them
# in environment variables.
# So before launching this test tester must run:
# $ export G5K_USERNAME=username
# $ read -s G5K_PASSWORD
# $ export G5K_PASSWORD
# $ export G5K_PASSWORD=$(cat ~/.ssh/id_rsa.pub)
# WARNING Be sure to use id_rsa.pub and not id_rsa
$username = $ENV{'G5K_USERNAME'};
$password = $ENV{'G5K_PASSWORD'};
$ssh_key = $ENV{'G5K_PUBLIC_KEY'};

$verbose = shift @ARGV;

$hash = get_power_and_deploy_capable_available_nodes_sold_by_the_devil(
	$username, $password, '00:10');

is(reftype $hash, reftype {},
	'get_power_and_deploy_capable_available_nodes_sold_by_the_devil return type');

foreach my $it (keys %{$hash}){
	print "$it:\n" if $verbose;

	foreach my $jt (@{$hash->{$it}}){
		print "\t* $jt\n" if $verbose;

		$thread = threads->create(\&test_thread, {username => $username,
			password => $password, site => $it, node => $jt, 'time' => '00:10',
			ssh_key => $ssh_key});

		push @threads, $thread;
	}
}

foreach my $it (@threads){
	$it->join();
}

done_testing();