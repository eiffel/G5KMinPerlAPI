#! /usr/bin/env perl
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
use strict;
use warnings;
use Scalar::Util qw(reftype);
use Test::More;

BEGIN{
	use_ok('G5KMinPerlAPI::Common',
		qw(get_sites get_site_status get_clusters get_nodes));
}

my $username;
my $password;

my $verbose;

my %GRID5000;
my @SITES;
my @CLUSTERS;

my $site;
my @sites;
my $site_status;
my @clusters;
my @nodes;

# Manually created GRID'5000 hash of array to check the result of the
# G5KMinPerlAPI::Common subroutines.
# The architecture is the following :
# * First levels are the sites
# * Second levels are the clusters belonging to a site.
%GRID5000 = (
	'grenoble' => ['edel', 'genepi'],
	'lille' => ['chifflet', 'chetemi'],
	'luxembourg' => ['granduc', 'petitprince'],
	'lyon' => ['sagittaire', 'taurus', 'orion', 'hercule', 'nova'],
	'nancy' => ['graoully', 'graphene', 'graphique', 'graphite', 'griffon',
		'grimoire', 'grisou', 'grimani', 'grele'],
	'nantes' => ['econome', 'ecotype'],
# WARNING Temporary comment this line since Rennes is unavailable.
# 	'rennes' => ['paravance', 'parasilo', 'paranoia', 'parapluie', 'parapide'],
	'sophia' => ['sol', 'suno', 'uva', 'uvb']
);

# Test must be automated, so instead of prompting for credentials we hide them
# in environment variables.
# So before launching this test tester must run:
# $ export G5K_USERNAME=username
# $ read -s G5K_PASSWORD
# $ export G5K_PASSWORD
$username = $ENV{'G5K_USERNAME'};
$password = $ENV{'G5K_PASSWORD'};

$verbose = shift @ARGV;

# We sort the array so we can compare them cells by cells.
@sites = sort @{get_sites({username => $username, password => $password})};
@SITES = sort keys %GRID5000;

# If the API did not give us the same number of sites that the hash above
# contains we need to die.
is(@sites, @SITES, 'Sites number');

for(my $i = 0; $i < @sites; $i++){
	$site = $sites[$i];
	# We then compare each sites with the sites contained in the above hash. If
	# one is different we die.
	is($site, $SITES[$i], 'Site name');

	$site_status = get_site_status({username => $username, password => $password,
		site => $site});

	is(reftype $site_status, reftype {}, 'get_site_status return type');

	foreach my $key (keys %{$site_status->{nodes}}){
		# We first test if the key is not empty because the regex will fail if it is
		# the case.
		# This test is mainly here to prevent the empty key of lille request.
		isnt($key, q{}, 'Node\'s name not empty');
		like($key, qr/$site/, "$key belongs to $site");
	}

	print $sites[$i] . "\n" if($verbose);

	@clusters = sort @{get_clusters({username => $username, password => $password,
		site => $sites[$i]})};
	@CLUSTERS = sort @{$GRID5000{$SITES[$i]}};

	# If the API did not give us the same number of clusters that the hash above
	# has we need to die.
	is(@clusters, @CLUSTERS, 'Clusters number');

	for(my $j = 0; $j < @clusters; $j++){
		# Compare each clusters to the ones which are in the above hash.
		is($clusters[$j], $CLUSTERS[$j], 'Cluster name');

		print "\t" . $clusters[$j] . "\n" if($verbose);

		@nodes = @{get_nodes({username => $username, password => $password,
			site => $sites[$i], cluster => $clusters[$j]})};

		# WARNING We will not compare the nodes provided by the API because they are
		# too much subject to changes.
		# One test could be to get them with (bash) `curl` and compare the result.
		# This test has not a lot of meaning since this Perl code also use curl.
		if($verbose){
			foreach my $it (@nodes){
				print "\t\t* $it\n";
			}
		}
	}
}

done_testing();