#! /usr/bin/env perl
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
use strict;
use warnings;
use Scalar::Util qw(reftype);
use Test::More;

BEGIN{
	plan tests => 14;
	use_ok('G5KMinPerlAPI::Common', qw(get_node_characteristics get_site_status));
	use_ok('G5KMinPerlAPI::Capable', qw(is_API_and_not_shared_power_capable
		is_deploy_capable is_sold_by_the_devil is_available_and_not_reserved));
}

my $username;
my $password;

my $json;

# Test must be automated, so instead of prompting for credentials we hide them
# in environment variables.
# So before launching this test tester must run:
# $ export G5K_USERNAME=username
# $ read -s G5K_PASSWORD
# $ export G5K_PASSWORD
$username = $ENV{'G5K_USERNAME'};
$password = $ENV{'G5K_PASSWORD'};

$json = get_node_characteristics({username => $username, password => $password,
	site => 'lyon', cluster=> 'sagittaire', node => 'sagittaire-1'});

is(reftype $json, reftype {}, 'get_node_characteristics return type');
ok(is_API_and_not_shared_power_capable($json),
	'sagittaire-1 in lyon is power capable');
ok(is_deploy_capable($json), 'sagittaire-1 in lyon is deploy capable');
ok(!is_sold_by_the_devil($json), 'sagittaire-1 in lyon has an AMD CPU');

$json = get_node_characteristics({username => $username, password => $password,
	site => 'sophia', cluster => 'uva', node => 'uva-1'});

is(reftype $json, reftype {}, 'get_node_characteristics return type');
ok(!is_API_and_not_shared_power_capable($json),
	'uva-1 in sophia is not power capable');
ok(is_deploy_capable($json), 'uva-1 in sophia is deploy capable');
ok(is_sold_by_the_devil($json), 'uva-1 in sophia has an Intel CPU');

$json = get_site_status({username => $username, password => $password,
	site => 'nancy'});

is(reftype $json, reftype {}, 'get_node_characteristics return type');
ok(!is_available_and_not_reserved({json => $json, site => 'nancy',
		node => 'griffon-79'}, 'griffon-79 in nancy is dead'));

$json = get_site_status({username => $username, password => $password,
	site => 'grenoble'});

ok(reftype $json, reftype {});
ok(!is_available_and_not_reserved({json => $json, site => 'grenoble',
		node => 'edel-70'}), 'edel-70 in grenoble is dead');