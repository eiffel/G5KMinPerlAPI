#! /usr/bin/env perl
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
use strict;
use warnings;
use Scalar::Util qw(reftype);
use Test::More;
use Readonly;
use Carp;

BEGIN{
	plan tests => 20;
	use_ok('G5KMinPerlAPI::Jobs', qw(reserve_deploy_node delete_job do_deploy));
	use_ok('G5KMinPerlAPI::Common', qw(get_sites get_clusters get_nodes
		get_job_characteristics get_deploy_characteristics get_site_status
		get_node_characteristics));
	use_ok('G5KMinPerlAPI::Capable', qw(is_running is_deployed is_deploy_capable
		is_available_and_not_reserved));
	use_ok('G5KMinPerlAPI::Experiments', qw(to_seconds));
}

# A JSON field which contains an uniq identifier.
Readonly my $UID => 'uid';

# Another JSON which contains the username of the job's owner.
Readonly my $USER_UID => 'user_uid';

# Remixed version of
# Experiments::get_power_and_deploy_capable_available_nodes_sold_by_the_devil
#
# This subroutine return a pair {site, node} whom node is deploy capable and
# available for the given time.
#
# This subroutine is not cool because it is a big copy/paste but it permits to
# automate this test because it will get a {site, node} by itself.
sub get_deploy_capable_available_node{
	my $username;
	my $password;
	my $job_time;

	my $sites;
	my $clusters;
	my $nodes;

	my $json_site;
	my $json_node;

	$username = shift @_ or croak 'Must provide 3 arguments (first is a string)';
	$password = shift @_ or croak 'Must provide 3 arguments (second is a string)';
	$job_time = shift @_
		or croak 'Must provide 3 arguments (third is an integer)';

	$sites = get_sites({username => $username, password => $password});

	if(reftype $sites ne reftype []){
		die 'get_sites return an array reference';
	}

	# Translate a classical job duration (HH[:MM[:SS]]) in second.
	$job_time = to_seconds($job_time);

	foreach my $it (@{$sites}){
		$clusters = get_clusters({username => $username, password => $password,
			site => $it});

		if(reftype $clusters ne reftype []){
			die 'get_clusters return an array reference';
		}

		$json_site = get_site_status({username => $username, password => $password,
			site => $it});

		foreach my $jt (@{$clusters}){
			$nodes = get_nodes({username => $username, password => $password,
			site => $it, cluster => $jt});

			if(reftype $nodes ne reftype []){
				die 'get_nodes return an array reference';
			}

			foreach my $kt (@{$nodes}){
				$json_node = get_node_characteristics({username => $username,
					password => $password, site => $it, cluster => $jt, node => $kt});

				if(is_available_and_not_reserved({json => $json_site, site => $it,
						node => $kt, job_time => $job_time})
					and is_deploy_capable($json_node)){
					return {$it, $kt};
				}
			}
		}
	}

	return undef;
}

my $username;
my $password;

my $hash;

my $site;
my $node;

my $verbose;

my $json;
my $job_id;
my $deploy_id;

# Test must be automated, so instead of prompting for credentials we hide them
# in environment variables.
# So before launching this test tester must run:
# $ export G5K_USERNAME=username
# $ read -s G5K_PASSWORD
# $ export G5K_PASSWORD
$username = $ENV{'G5K_USERNAME'};
$password = $ENV{'G5K_PASSWORD'};

$verbose = shift @ARGV;

$hash = get_deploy_capable_available_node($username, $password, '00:20');

is(reftype $hash, reftype {}, 'get_deploy_capable_available_node return type');
ok(keys %{$hash}, 'Returned hash is not empty');

# keys return an array so to access the first key we just get the first call of
# the returned array.
$site = (keys %{$hash})[0];
$node = $hash->{$site};

$json = reserve_deploy_node({username => $username, password => $password,
	site => $site, node => $node, ressources => 'walltime=00:20:00'});

is(reftype $json, reftype {}, 'reserve_deploy_node return type');
ok(length $json->{$UID}, "$UID field is not empty");
ok(length $json->{$USER_UID}, "$USER_UID field is not empty");
is($json->{$USER_UID}, $username, "$USER_UID is $username");

print "reserve_deploy_node:\n", %{$json}, "\n\n" if($verbose);

$job_id = $json->{$UID};

# We sleep 60 seconds to let the time to the job to be in running state.
sleep 60;

$json = get_job_characteristics({username => $username, password => $password,
	site => $site, job_id => $job_id});

is(reftype $json, reftype {}, 'get_job_characteristics return type');
is($json->{$UID}, $job_id, $json->{$UID} . " is $job_id");
ok(is_running($json), 'Job must be running');

$json = do_deploy({username => $username, password => $password,
	site => $site, node => $node, environment => 'debian9-x64-min',
	ssh_key => 'DO_NOT_CARE'});

is(reftype $json, reftype {}, 'do_deploy return type');

$deploy_id = $json->{$UID};

print "do_deploy:\n", %{$json}, "\n\n" if($verbose);

$json = get_deploy_characteristics({username => $username,
	password => $password, site => $site, deploy_id => $deploy_id});

is(reftype $json, reftype {}, 'get_deploy_characteristics return type');
is($json->{$UID}, $deploy_id, "$json->{$UID} is $deploy_id");
# Deployment takes more time, it cannot be finished now.
ok(!is_deployed($json), 'Deployement takes more time');

# We sleep 600 seconds to let the time to the deployment to finish.
sleep 600;

# We need to reload the characteristics of the deployment to see if it is marked
# as terminated.
$json = get_deploy_characteristics({username => $username,
	password => $password, site => $site, deploy_id => $deploy_id});

ok(is_deployed($json), 'Deployment must be terminated');

print "get_deploy_characteristics:\n", %{$json}, "\n\n" if($verbose);

# We ask the API to delete the job we just created.
delete_job({username => $username, password => $password, site => $site,
	job_id => $job_id});

# We sleep 30 seconds to let the time to be in "dead" state.
sleep 30;

# We need to reload the characteristics of the job to see if it is marked as
# dead.
$json = get_job_characteristics({username => $username, password => $password,
	site => $site, job_id => $job_id});

is(reftype $json, reftype {}, 'get_job_characteristics return type');
ok(!is_running($json), 'Job was deleted, it must not be in the running state');