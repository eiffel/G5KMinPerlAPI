# Copyright (c) 2018 Francis Laniel <francis.laniel@lip6.fr>
#
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
package G5KMinPerlAPI::SSHez;
use strict;
use warnings;
use Carp;
use Readonly;
use Scalar::Util qw(reftype);
use Net::OpenSSH;
use File::Basename;
require Exporter;
use base qw(Exporter);

our @EXPORT_OK = qw(make_ssh_tunnel make_site_to_node_tunnel
	put_file_in_remote_home get_file_in_local_wd);

# ssh port is 22.
Readonly our $SSH_PORT => 22;

# The URL of GRID5000 network, it is used to reform complete gateway, site or
# node name.
Readonly our $GRID5000_URL => 'grid5000.fr';

# File use to store key obtained by ssh-keyscan. It will be used as input of
# ssh-keygen.
Readonly our $KEY_FILE => 'key_file';

# The number of times we try to recreate the node public hash key.
# This variable is here because there is an odd behavior, for more details
# please take a look at the commentaries inside _recreate_node_public_key.
Readonly our $TRIES => 3;

# The public key hash is made with sha256 algorithm.
Readonly our $SHA256 => 'SHA256';

# We want to connect to a deployed node as the root user. This global variable
# contains the name of this user.
Readonly our $ROOT => 'root';

# Timeout of 30 seconds used by the select subroutine.
Readonly our $TIMEOUT => 30;

# Size of the data we want to read from the pseudo-TTY.
Readonly our $SIZE => 1024;

# String used to answer to "The authenticity of host 'hostname' can't be
# established.".
Readonly our $YES => 'yes';

# String used to answer to "The authenticity of host 'hostname' can't be
# established.".
Readonly our $NO => 'no';

# There is no other place like ~.
Readonly our $HOME => '~';

# Make the first part of the tunnel: from the gate toward the site.
#
# This subroutine is private and will not be exported.
#
# ALERT The user of this module should have connect once to all the site because
# it is not possible to dynamically accept a new site. If it is not the case
# the user will be prompted to accept or not (yes/no) the unknown sites.
#
# @param $arg_ref A reference to an hash whom fields are:
# @param username The username used to connect to GRID'5000.
# @param gateway The name of the gateway it is likely to be 'access'.
# @param site The name of the site we want to connect (e.g. toulouse).
# @return A reference to an hash which contains the two ssh objects. Indeed, if
# we lost a reference to the gateway one the site one will not work anymore if
# the GC ask for the gateway memory.
sub _make_gateway_to_site_tunnel{
	my $arg_ref;

	my $username;
	my $gateway;
	my $site;
	my $node;

	my $ssh_gateway;
	my $ssh_site;
	my $proxy_cmd;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$gateway = $arg_ref->{gateway} or croak 'Field gateway is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';

	# Reform the complete names.
	$gateway .= ".$GRID5000_URL";
	$site .= ".$GRID5000_URL";

	# Create the new ssh connection to the gateway.
	$ssh_gateway = Net::OpenSSH->new($gateway,
		(user => $username, port => $SSH_PORT, timeout => 3600));
	$ssh_gateway->error()
		and die 'Could not establish SSH connection: ' . $ssh_gateway->error();

	# This subroutine call returns a string that can be used to do tunnel ssh.
	$proxy_cmd = $ssh_gateway->make_remote_command({tunnel => 1}, $site,
		$SSH_PORT) or die 'Problem with remote command: ' . $ssh_gateway->error();

	# We used the previous $proxy_cmd in this new ssh to create a tunnel.
	$ssh_site = Net::OpenSSH->new($site, proxy_command => $proxy_cmd,
		(user => $username, port => $SSH_PORT));
	$ssh_site->error()
		and die 'Could not establish SSH connection: ' . $ssh_site->error();

	return {ssh_gateway => $ssh_gateway, ssh_site => $ssh_site};
}

# This method recreate the hash of a node public key.
#
# This subroutine is private and will not be exported.
#
# WARNING This subroutine can die if the key could not been recreated, if you
# are here you should take a look at the "retry" loop inside the subroutine to
# get more information.
#
# @param $ssh_site A reference to a Net::OpenSSH object.
# @param $node The complete name of the node we want to recreated the public
# key.
# @return The public key of the node.
# @die If the public key was not found.
sub _recreate_node_public_key{
	my $ssh_site;
	my $node;

	my $key_file;

	my $i;

	my $out;

	my $public_hash_key;

	$ssh_site = shift @_ or croak 'Must provide two arguments';
	$node = shift @_ or croak 'Must provide two arguments';

	# TODO test if $ssh_site is a reference to Net::OpenSSH->new().

	# Sometimes we launch multiple experiments at the same time.
	# Those experiments will all pass by this function so they will share
	# key_file.
	# We have to make it uniq so we just append the node name to the key_file.
	$key_file = $KEY_FILE . $node;

	for($i = 0; $i < $TRIES; $i++){
		# This command was found at https://superuser.com/a/1111974
		# The goal of this command is to ask the node its public key and recompute
		# its hash.
		$ssh_site->system("ssh-keyscan -t rsa $node > $key_file")
			or die 'Remote command failed: ' . $ssh_site->error();
		$out = $ssh_site->capture("ssh-keygen -lf $key_file");

		# Sometime for unknown reasons the ssh-keyscan command prints nothing
		# so ssh-keygen failed with printing '(stdin) is not a public key file.'
		# (this behavior also occurs on alive nodes).
		# To avoid to die too quickly we decide to try this command $TIMES times.
		# If at one iteration there is no error then we exit the loop, otherwise
		# we sleep 30 seconds and we retry.
		# If we executed the loop $TIMES times we decide to die as it is done in the
		# below if (the one outside the loop).
		if(not $ssh_site->error()){
			last;
		}else{
			warn "Try number $i/$TRIES: Capture failed: " . $ssh_site->error()
				. "\nWe will retry in $TIMEOUT seconds";
			sleep $TIMEOUT;
		}
	}

	if($i == $TRIES){
		die "Try number $i/$TRIES: Capture failed: " . $ssh_site->error();
	}

	if($out =~ /$SHA256:(.+) $node/){
		$public_hash_key = $1;
	}else{
		die "No key was found in $out";
	}

	return $public_hash_key;
}

# This subroutine returns an anonymous subroutine.
# The anonymous subroutine is used a subroutine reference as a callback.
# This subroutine is basically a trick to add a fourth argument to the
# login_handler callback.
#
# This subroutine is private and will not be exported.
#
# @param $public_hash_key The hash of the public key got by
# _recreate_node_public_key.
# @return An anonymous subroutine which call __login_handler with
# $public_hash_key as the last argument.
sub _login_handler{
	my $public_hash_key;

	$public_hash_key = shift @_ or croak 'Must have one argument';

	return sub {__login_handler(@_, $public_hash_key);};
}

# This subroutine is the callback given as login_handler.
# It will be called during the creation of the last ssh tunnel
# ((access -> site)-> node).
# Its goal is to handle appearance of message like:
# "The authenticity of host 'hostname' can't be established."
# If this message appears it gives the hash of the public key, this hash is
# compared with one given as last argument.
# If they are equals the subroutine returns 1 and trust the distant node.
#
# WARNING If no message appeared then the node is considered as already known
# and trusted since this kind of message only occurs when the machine is unknown
# (or if the key changed).
#
# This subroutine is private and will not be exported.
#
# @param $ssh The newly created ssh, in our case it is the tunnel
# ((access -> site) -> node).
# @param $pty A pseudo-TTY which permits to interact with the distant ssh. It is
# it which will contain the message and which we interact with.
# @param $unused A reference to a scalar, we do not use it.
# @return 1 if we trust the distant host, 0 otherwise.
sub __login_handler{
	my $ssh;
	my $pty;
	my $unused;
	my $public_hash_key;

	my $data;
	my $key;

	my $rin;
	my $nfound;
	my $read;

	$ssh = shift @_ or croak 'Must have 4 arguments';
	$pty = shift @_ or croak 'Must have 4 arguments';
	$unused = shift @_ or croak 'Must have 4 arguments';
	$public_hash_key = shift @_ or croak 'Must have 4 arguments';

	# We create a bit vector to be used by the select subroutine.
	$rin = q{};
	vec($rin, $pty->fileno(), 1) = 1;

	# We wait 30 seconds on $pty->fileno.
	$nfound = select $rin, undef, undef, $TIMEOUT;

	# If at the end of the timeout there was nothing to read we make the
	# assumption that we already met this machine and we already trust it.
	if(not $nfound){
		return 1;
	}

	# This was copied from: https://searchcode.com/codesearch/view/19392796/
	$read = sysread $pty, $data, $SIZE;

	# If we arrive here it means that the pty contains a message like
	# "The authenticity of host 'hostname' can't be established.".
	if($read){
		if($data =~ /$SHA256:(.*)\./){
			$key = $1;

			if($public_hash_key eq $key){
				# If the key given through $pty is the same than the one given as
				# argument we continue the connection and trust the distant host.
				print {$pty} "$YES\n" or die "Failed to print to \$pty: $!";

				return 1;
			}else{
				# If the key given through $pty is different than the one given as
				# argument we do not continue the connection.
				print {$pty} "$NO\n" or die "Failed to print to \$pty: $!";

				return 0;
			}
		}
	}else{
		# The sysread executed badly, it is not a good sign for the rest of the
		# execution.
		return 0;
	}
}

sub make_site_to_node_tunnel{
	my $ssh_site;
	my $site;
	my $node;

	my $public_hash_key;
	my $proxy_cmd;

	my $ssh_node;

	$ssh_site = shift @_
		or croak 'Must provide 3 arguments (1st is a Net::OpenSSH object)';
	$site = shift @_ or croak 'Must provide 3 arguments (2nd is a string)';
	$node = shift @_ or croak 'Must provide 3 arguments (3rd is a string)';

	# Reform the complete node name.
	$node .= ".$site.$GRID5000_URL";

	$public_hash_key = _recreate_node_public_key($ssh_site, $node);

	# This subroutine call returns a string that can be used to do tunnel ssh.
	$proxy_cmd = $ssh_site->make_remote_command({tunnel => 1}, $node, $SSH_PORT)
		or die 'Problem with remote command: ' . $ssh_site->error();

	# We create the (access -> site) -> node tunnel.
	# We give __login_handler as the callback to handle message like
	# "The authenticity of host 'hostname' can't be established.".
	$ssh_node = Net::OpenSSH->new($node, proxy_command => $proxy_cmd,
		(user => $ROOT, port => $SSH_PORT, timeout => 3600,
		login_handler => _login_handler($public_hash_key)));

	# TODO transform this die in something other (e.g. returning ssh_hash without
	# ssh_node) because there is an unknown error "Host key verification failed."
	$ssh_node->error()
		and die 'Could not establish SSH connection: ' . $ssh_node->error();

	return $ssh_node;
}

sub make_ssh_tunnel{
	my $arg_ref;

	my $username;
	my $gateway;
	my $site;
	my $node;

	my $ssh_hash;
	my $ssh_site;
	my $ssh_node;
	my $proxy_cmd;

	my $public_hash_key;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$gateway = $arg_ref->{gateway} or croak 'Field gateway is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$node = $arg_ref->{node} or croak 'Field node is empty';

	# We first create the access -> site tunnel.
	$ssh_hash = _make_gateway_to_site_tunnel({username => $username,
		gateway => $gateway, site => $site});

	if(reftype $ssh_hash ne reftype {}){
		croak 'make_gateway_to_site_tunnel must return an hash reference';
	}

	$ssh_site = $ssh_hash->{ssh_site} or croak 'Field ssh_site is empty';

	# We add the $ssh_node to the hash so our hash contains all the ssh created.
	$ssh_hash->{ssh_node} = make_site_to_node_tunnel($ssh_site, $site, $node);

	$ssh_hash->{ssh_node} or croak 'Field ssh_node is empty';

	return $ssh_hash;
}

sub put_file_in_remote_home{
	my $ssh;
	my $file;

	my $sftp;
	my $rfile;

	$ssh = shift @_ or croak 'Must provide 2 arguments.';
	$file = shift @_ or croak 'Must provide 2 arguments.';

	$sftp = $ssh->sftp();
	$sftp->die_on_error('Unable to establish SFTP connection');

	$rfile = basename($file);

	$sftp->setcwd($HOME);

	if(not $sftp->put($file, $rfile)){
		warn 'Put failed: ' . $sftp->error();

		return 0;
	}

	return 1;
}

sub get_file_in_local_wd{
	my $ssh;
	my $rfile;

	my $sftp;
	my $file;

	$ssh = shift @_ or croak 'Must provide 2 arguments.';
	$rfile = shift @_ or croak 'Must provide 2 arguments.';

	$sftp = $ssh->sftp();
	$sftp->die_on_error('Unable to establish SFTP connection');

	$file = basename($rfile);

	if(not $sftp->get($rfile, $file)){
		warn 'Get failed: ' . $sftp->error();

		return 0;
	}

	return 1;
}

1;

__END__

=pod

=head1 NAME

G5KMinPerlAPI::SSHez - Perl module to ssh easily to the GRID'5000 network?

=head1 SYNOPSIS

	use G5KMinPerlAPI::SSHez;

	#...

	$ssh_hash = make_ssh_tunnel({username => $username, gateway => $gateway,
		site => $site, node => $node});

	$ssh_hash->{ssh_node}->system('echo hello');

=head1 DESCRIPTION

G5KMinPerlAPI::SSHez is a Perl module which permits user of the GRID'5000
network to ssh to previously reserved nodes.

=head1 SUBROUTINES

=over 4

=item C<< make_ssh_tunnel HASH_REF >>

This subroutine creates an ssh tunnel from the acces gateway to the node:
access -> site -> node

It returns an hash reference which contains the 3 ssh objets which were created
to build the tunnel. The more interessting is the third one which is labeled by
'ssh_node' and can be used to execute remote command.
The others ssh objects (labeled 'ssh_gateway' and 'ssh_site') B<< must >> not
been lost because if the garbage collector reclaim them the tunnel will be
broken.

The node B<< must >> have been reserved previously.

The hash reference B<< must >> contain the following field:

=over 4

=item C<< username >>

This field will contain the username required by the request. This is a lack of
genericity but since this module is thought to be used on GRID'5000 we need a
username to connect the network.

=item C<< gateway >>

The name of the gateway. For GRID'5000 it is access.
Its full name "access.grid5000.fr" will be recreated by an inner call.

=item C<< site >>

The name of the site, its complete name "site.grid5000.fr" will be rewritten by
an inner call.

=item C<< node >>

The name of the node we want to ssh.
Its complete name "node.site.grid5000.fr" will also be reformed by an inner
call.

=back

=item C<< make_site_to_node_tunnel SSH, SITE, NODE>>

This subroutine creates the site -> node tunnel.

It returns a reference to an OpenSSH object.

=item C<< put_file_in_remote_home SSH, FILENAME >>

This subroutine will put filename in the remote home associated to the ssh given
as first argument.

It will basically use sftp to put the file. This subroutine can die if it failed
to establish the SFTP connection.

This subroutine returns 1 if the file was effectively put or 0 if there was an
error.

=item C<< get_file_in_local_wd SSH, FILENAME >>

This subroutine will get filename in the remote home associated to the ssh given
as first argument and stores it in the local current working directory.

It will basically use sftp to get the file. This subroutine can die if it failed
to establish the SFTP connection.

This subroutine returns 1 if the file was effectively got or 0 if there was an
error.

=back

=head1 DIAGNOSTICS

If you forgot to provide the hash reference needed as argument the subroutine
will croak on you.

It will also croak on you if you forgot a mandatory field.

Due to an inner call to a non exported subroutine it is possible to die.

=head1 DEPENDENCIES

This module uses the following Perl module:

=over 4

=item L<< Carp >>

This module is needed because G5KMinPerlAPI::Common croak on the user if she/he
gave a bad number of parameters to the subroutines.

=item L<< Readonly >>

G5KMinPerlAPI::Common defines several constant. We use this module to define
them.

=item L<< Scalar::Util >>

This module it mainly used to check if the JSON given as argument is formated as
expected.

=item L<< Net::OpenSSH >>

G5KMinPerlAPI::SSHez is basically an easy wrapper for Net::OpenSSH.

=item L<< File::Basename >>

This module is used to compute the basename of the file used by
put_file_in_remote_home and get_file_in_local_wd.

=item L<< Exporter >>

G5KMinPerlAPI::Common of course exports variables and subroutine. It requires
L<< Exporter >>to do so.

=item L<< base >>

L<< base >> and L<< Exporter >> are used together to exportation.

=back

=head1 INCOMPATIBILITIES

Until now there is no known incompatibility.

=head1 AUTHOR

Francis Laniel (francis.laniel@lip6.fr)

=head1 LICENSE AND COPYRIGHT

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This module is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut