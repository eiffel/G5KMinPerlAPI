# Copyright (c) 2018 Francis Laniel <francis.laniel@lip6.fr>
#
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
package G5KMinPerlAPI::Experiments;
use strict;
use warnings;
use Carp;
use Readonly;
use Scalar::Util qw(reftype);
use G5KMinPerlAPI::Common qw(get_sites get_site_status get_clusters get_nodes
	get_node_characteristics get_job_characteristics get_deploy_characteristics
	get_node_power_consumption);
use G5KMinPerlAPI::Capable qw(is_API_and_not_shared_power_capable
	is_available_and_not_reserved is_deploy_capable is_sold_by_the_devil
	is_running is_deployed);
use G5KMinPerlAPI::Jobs qw(reserve_deploy_node do_deploy);
use G5KMinPerlAPI::SSHez qw(make_ssh_tunnel make_site_to_node_tunnel
	put_file_in_remote_home);
require Exporter;
use base qw(Exporter);

our @EXPORT_OK = qw(
	get_power_and_deploy_capable_available_nodes_sold_by_the_devil
	reserve_deploy_and_connect_job launch_script_and_get_power to_seconds);

# There are 3600 seconds (60 * 60) in one hour.
Readonly our $SECONDS_IN_HOUR => 3600;

# One minute contains 60 seconds.
Readonly our $SECONDS_IN_MINUTE => 60;

# Walltime property for reservation.
Readonly our $WALLTIME => 'walltime=';

# JSON field which contains the name of the node.
Readonly our $UID => 'uid';

# Environment we want to reserve. For now it is written hard in the code but it
# is not hard to add it as an argument.
Readonly our $ENVIRONMENT => 'debian9-x64-min';

# The name of the gate of GRID'5000.
Readonly my $GATEWAY => 'access';

# JSON field which contains the timestamp and the values of power consumption.
Readonly our $ITEMS => 'items';

# JSON field which contains the timestamp where power consumption was measured.
Readonly our $TIMESTAMPS => 'timestamps';

# JSON field which contains the power measured over the time.
Readonly our $VALUES => 'values';

sub to_seconds{
	my $time;

	$time = shift @_ or croak 'Must provide 1 argument';

	# Match HH:MM:SS
	if($time =~ /(\d+):(\d+):(\d+)/){
		# return HH * 3600 + MM * 60 + SS;
		return $1 * $SECONDS_IN_HOUR + $2 * $SECONDS_IN_MINUTE + $3;
	}

	# Match HH:MM
	if($time =~ /(\d+):(\d+)/){
		# return HH * 3600 + MM * 60;
		return $1 * $SECONDS_IN_HOUR + $2 * $SECONDS_IN_MINUTE;
	}

	# Match HH
	if($time =~ /(\d+)/){
		# return HH * 3600;
		return $1 * $SECONDS_IN_HOUR;
	}

	croak "$time is not well formated, supported format are: HH, HH:MM and HH:MM:SS";
}

sub get_power_and_deploy_capable_available_nodes_sold_by_the_devil{
	my $username;
	my $password;
	my $job_time;

	my $sites;
	my $clusters;
	my $nodes;

	my $json_site;
	my $json_node;

	my $ret;

	$username = shift @_ or croak 'Must provide 3 arguments (first is a string)';
	$password = shift @_ or croak 'Must provide 3 arguments (second is a string)';
	$job_time = shift @_
		or croak 'Must provide 3 arguments (third is an integer)';

	$sites = get_sites({username => $username, password => $password});

	if(reftype $sites ne reftype []){
		die 'get_sites return an array reference';
	}

	$ret = {};

	# Translate a classical job duration (HH[:MM[:SS]]) in second.
	$job_time = to_seconds($job_time);

	foreach my $it (@{$sites}){
		$clusters = get_clusters({username => $username, password => $password,
			site => $it});

		if(reftype $clusters ne reftype []){
			die 'get_clusters return an array reference';
		}

		$json_site = get_site_status({username => $username, password => $password,
			site => $it});

		# The above subroutine call can return undef.
		# If it is the case we pass to the next site.
		if(not defined $json_site){
			next;
		}

		foreach my $jt (@{$clusters}){
			$nodes = get_nodes({username => $username, password => $password,
			site => $it, cluster => $jt});

			if(reftype $nodes ne reftype []){
				die 'get_nodes return an array reference';
			}

			foreach my $kt (@{$nodes}){
				$json_node = get_node_characteristics({username => $username,
					password => $password, site => $it, cluster => $jt, node => $kt});

				if(defined $json_node
					and is_API_and_not_shared_power_capable($json_node)
					and is_available_and_not_reserved({json => $json_site, site => $it,
						node => $kt, job_time => $job_time})
					and is_sold_by_the_devil($json_node)
					and is_deploy_capable($json_node)){
					push @{$ret->{$it}}, $kt;
				}
			}
		}
	}

	return $ret;
}

sub reserve_deploy_and_connect_job{
	my $arg_ref;

	my $username;
	my $password;
	my $site;
	my $node;
	my $time;
	my $ssh_key;

	my $hash;

	my $job_id;
	my $deploy_id;

	my $ret;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$node = $arg_ref->{node} or croak 'Field node is empty';
	$time = $arg_ref->{'time'} or croak 'Field time is empty';
	$ssh_key = $arg_ref->{ssh_key} or croak 'Field ssh_key is empty';

	$hash = reserve_deploy_node({username => $username, password => $password,
		site => $site, node => $node, ressources => "$WALLTIME$time"});

	if(not defined $hash){
		return undef;
	}

	if(reftype $hash ne reftype {}){
		die 'reserve_deploy_node must return an hash reference';
	}

	$job_id = $hash->{$UID};

	# Add the job_id to the returned hash so the user can delete the job.
	$ret->{job_id} = $job_id;

	do{
		# We sleep 30 seconds to let the time to the job to be in running state.
		sleep 30;

		$hash = get_job_characteristics({username => $username,
			password => $password, site => $site, job_id => $job_id});
		# If the job is still not running we will sleep 30 seconds again and again.
	}while(not is_running($hash));

	$hash = do_deploy({username => $username, password => $password, site => $site,
		node => $node, environment => $ENVIRONMENT, ssh_key => $ssh_key});

	if(not defined $hash){
		warn 'Something went wront during deployment, you should kill your job.';

		return $ret;
	}

	if(reftype $hash ne reftype {}){
		die 'do_deploy must return an hash reference';
	}

	$deploy_id = $hash->{$UID};

	do{
		# We sleep 30 seconds to let the deployment finish.
		sleep 30;

		$hash = get_deploy_characteristics({username => $username,
			password => $password, site => $site, deploy_id => $deploy_id});
		# If the deployment is not finished we will sleep 30 seconds again and
		# again.
	}while(not is_deployed($hash));

	$hash = make_ssh_tunnel({username => $username, gateway => $GATEWAY,
		site => $site, node => $node});

	if(reftype $hash ne reftype {}){
		die 'make_ssh_tunnel must return an hash reference';
	}

	return {%{$ret}, %{$hash}};
}

# Translate a specific JSON item (containing two arrays) to a CSV array.
# Each cell of the array contains a string likewise "time, value".
#
# This subroutine is private and will not be exported.
#
# @param json A reference to an hash.
# @return An array of CSV string.
sub _json_to_csv{
	my $json;

	my $timestamps;
	my $values;

	my @ret;

	$json = shift @_ or croak 'Must provide 1 argument (a reference to an hash)';

	if(reftype $json ne reftype {}){
		die 'Argument must be an hash reference';
	}

	# Get the first item.
	$json = $json->{$ITEMS}[0];

	$timestamps = $json->{$TIMESTAMPS};
	$values = $json->{$VALUES};

	if(reftype $timestamps ne reftype [] or reftype $values ne reftype []){
		die '$timestamps and $values must array references';
	}

	if(scalar @{$timestamps} != scalar @{$values}){
		die 'Array timestamps and values must have the same length';
	}

	# For each timestamp and its associated value we create a string that we add
	# to our @ret array.
	for(my $i = 0; $i < scalar @{$values}; $i++){
		push @ret, $timestamps->[$i] . ", " . $values->[$i] . "\n";
	}

	return @ret;
}

sub launch_script_and_get_power{
	my $arg_ref;

	my $username;
	my $password;
	my $site;
	my $node;
	my $ssh;
	my $command;
	my $fd_out;
	my $fd_err;

	my $before_command;

	my $out;
	my $err;

	my $json;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$node = $arg_ref->{node} or croak 'Field node is empty';
	$ssh = $arg_ref->{ssh} or croak 'Field ssh is empty';
	$command = $arg_ref->{command} or croak 'Field command is empty';
	$fd_out = $arg_ref->{fd_out} or croak 'Field fd_out is empty';
	$fd_err = $arg_ref->{fd_err} or croak 'Field fd_err is empty';

	# We want to get the power consumption of the command so weed to register
	# its beginning time.
	$before_command = time;

	# Effectively execute the command.
	($out, $err) = $ssh->capture2($command);

	print {$fd_out} $out;
	print {$fd_err} $err;

	$json = get_node_power_consumption({username => $username,
		password => $password, site => $site, node => $node,
		from => $before_command, to => time});

	if(defined $json){
		print {$fd_out} _json_to_csv($json);
	}else{
		warn 'get_node_power_consumption return undef nothing will be written in $fd_out';

		print {$fd_err} 'get_node_power_consumption return undef nothing will be written in $fd_out';
	}
}

1;

__END__

=pod

=head1 NAME

G5KMinPerlAPI::Common - Perl module to start experiments on the GRID'5000
network.

=head1 SYNOPSIS

	TODO SYNOPSIS

=head1 DESCRIPTION

G5KMinPerlAPI::Experiments is a Perl module which permits user of the GRID'5000
network to start remote experiments on deployed node.

With this module you are able to launch script and get the power consumption of those script.

=head1 SUBROUTINES

=over 4

=item C<< to_second TIME >>

Translate a string representing time in second.

This subroutine takes as an argument a string formatted like that:

=over 4

=item * HH:MM:SS (e.g. 15:37:2)

=item * HH:MM (e.g. 4:52)

=item * HH (e.g. 7)

=back

For each format there must be one digit, the other is optionnal (no need to
write 04:52 to mean 4:52).

It will return the total duration in second (e.g. 4:52 -> 17520 seconds).

=item C<< get_power_and_deploy_capable_available_nodes_sold_by_the_devil USERNAME, PASSWORD, JOB_TIME >>

This subroutine returns an hash reference which will contains as keys the site
name an as values an array reference which will contain the power measurable,
deploy capable, available and sold by Intel node.

=item C<< reserve_deploy_and_connect_job HASH_REF >>

This subroutine will reserve a node to be deployed and will connect through ssh
to it.

It returns the an hash reference which contains all the ssh created to connect
to the node (for more information you can check
G5KMinPerlAPI::SSHez::make_ssh_tunnel subroutine).

It takes as argument an hash reference which B<< must >> contain the following
fields:

=over 4

=item username

The login you use to connect to GRID'5000.

=item password

The password associated to your username.

=item site

The node you want to reserve and connect belongs to a site, this must its parent
site.

=item node

The node you want to reserve and connect.

=item time

You want to reserve a node for a given duration.

=item ssh_key

Your ssh public key. B<< Beware >>, this must be you ssh public key and not the
path to your public key.

=back

=item C<< launch_script_and_get_power HASH_REF >>

This subroutine will execute on the remote node a script/command and will get
the power consumption of the whole node during the execution of the command.

It returns nothing but you will find useful information in the files refered by
fd_out and fd_err.

=over 4

=item username

The login you use to connect to GRID'5000.

=item password

The password associated to your username.

=item site

The node you want to reserve and connect belongs to a site, this must its parent
site.

=item node

The node you want to reserve and connect.

=item ssh

A reference to an OpenSSH object which is connect to the node.

=item command

The command you want to run.

=item fd_out

An already opened file descriptor which will contain the STDOUT of your command.

=item fd_err

An already opened file descriptor which will contain the STDERR of your command.

=back

=back

=head1 DIAGNOSTICS

If you provide a bad number of argument to subroutines they will croak on you
indicating the number of arguments required.

They will also croak on you if you gave a bad argument (e.g. an hash insteand
of an hash reference) and if you forgot a mandatory field for those hashes.

=head1 DEPENDENCIES

This module uses the following Perl module:

=over 4

=item L<< Carp >>

This module is needed because G5KMinPerlAPI::Common croak on the user if she/he
gave a bad number of parameters to the subroutines.

=item L<< Readonly >>

G5KMinPerlAPI::Common defines several constant. We use this module to define
them.

=item L<< Scalar::Util >>

This module it mainly used to check if the JSON given as argument is formated as
expected.

=item L<< Exporter >>

G5KMinPerlAPI::Common of course exports variables and subroutine. It requires
L<< Exporter >>to do so.

=item L<< base >>

L<< base >> and L<< Exporter >> are used together to exportation.

=item L<< G5KMinPerlAPI::Common >>

This module is used to get the sites, clusters and nodes.

=item L<< G5KMinPerlAPI::Capable >>

This module is used to get information of the ability of some nodes.

=item L<< G5KMinPerlAPI::Jobs >>

This module is used to launch and check if a job is running.

=item L << G5KMinPerlAPI::SSHez >>

This module is to connect to a remote job.

=back

=head1 INCOMPATIBILITIES

Until now there is no known incompatibility.

=head1 AUTHOR

Francis Laniel (francis.laniel@lip6.fr)

=head1 LICENSE AND COPYRIGHT

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This module is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut