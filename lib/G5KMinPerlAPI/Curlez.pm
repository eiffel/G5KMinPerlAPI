# Copyright (c) 2018 Francis Laniel <francis.laniel@lip6.fr>
#
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
package G5KMinPerlAPI::Curlez;
use strict;
use warnings;
use Carp;
use Readonly;
use Scalar::Util qw(reftype);
use WWW::Curl::Easy;
use Cpanel::JSON::XS;
require Exporter;
use base qw(Exporter);

our @EXPORT_OK = qw($GET $POST $DELETE curl_request);

Readonly our $GET => 16;
Readonly our $POST => 12;
Readonly our $DELETE => 1994;

# A global variable used by prepare_POST_request to indicate that we will send
# a JSON string with the request.
Readonly our $CONTENT_TYPE => 'Content-Type: application/json';

# A global variable used by prepare_DELETE_request to indicate to Curl that we
# will do a DELETE request.
Readonly our $DELETE_STRING => 'DELETE';

# A global variable which indicates that our GET request succedeed.
Readonly our $OK => 200;

# A global variable which indicates that our POST request succedeed. This is
# only true for deploy request.
Readonly our $CREATED => 201;

# A global variable which indicates that our DELETE request succedeed. This is
# only true for delete job request.
Readonly our $ACCEPTED => 202;

# Prepare the Curl object to make a request.
#
# This subroutine is generic and will be used by all the type of request (GET,
# POST and DELETE).
# At the end of it the Curl object returned can be used directly to perform a
# GET request.
#
# There is one parameter which is a reference to an hash which contains fields.
# Those field will be used as parameter and will be described below.
#
# @param $arg_ref A reference to Perl hash.
# @param username The username used to connect to GRID'5000.
# @param password The password of the corresponding username.
# @param url The url to query.
# @return A reference to a Perl hash which contains among other a reference to a
# curl object.
sub _prepare_request{
	my $arg_ref;

	my $username;
	my $password;
	my $url;

	my $curl;
	my $curl_ret;

	$arg_ref = shift @_
		or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	# Get the parameters.
	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$url = $arg_ref->{url} or croak 'Field url is empty';

	# This Perl hash reference will be returned
	$curl = {
		# This field will contain the reference to the Curl object.
		curl => q{},
		# This field will contain the header of the response.
		header => q{},
		# This field will contain the json of the response.
		json => q{},
		# This field contains the url to query.
		url => $url,
		# This field contains the expected return code.
		# If the returned code is different the request failed.
		success_code => $OK
	};

	$curl->{curl} = WWW::Curl::Easy->new();

	if(($curl_ret = $curl->{curl}->setopt(CURLOPT_USERPWD, "$username:$password"))
		!= CURLE_OK){
		die $curl->{curl}->strerror($curl_ret);
	}

	if(($curl_ret = $curl->{curl}->setopt(CURLOPT_URL, $url)) != CURLE_OK){
		die $curl->{curl}->strerror($curl_ret);
	}

	# We store the header and the json.
	if(($curl_ret = $curl->{curl}->setopt(CURLOPT_HEADERDATA, \$curl->{header}))
		!= CURLE_OK){
		die $curl->{curl}->strerror($curl_ret);
	}

	if(($curl_ret = $curl->{curl}->setopt(CURLOPT_WRITEDATA, \$curl->{json}))
		!= CURLE_OK){
		die $curl->{curl}->strerror($curl_ret);
	}

	return $curl;
}

# Prepare the Curl object to make a POST request.
#
# This subroutine is specific and should be used only for POST request.
# At the end of it the Curl object returned can be used directly to perform a
# POST request.
#
# There is one parameter which is a reference to an hash which contains fields.
# Those field will be used as parameter and will be described below.
#
# @param $arg_ref A reference to Perl hash.
# @param username The username used to connect to GRID'5000.
# @param password The password of the corresponding username.
# @param url The url to query.
# @param json A JSON string that will be send with the POST request.
# @return A reference to a Perl hash which contains among other a reference to a
# curl object.
sub _prepare_POST_request{
	my $arg_ref;

	my $username;
	my $password;
	my $url;
	my $json;

	my $curl;
	my $options;
	my $curl_ret;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	# Get the arguments.
	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$url = $arg_ref->{url} or croak 'Field url is empty';
	$json = $arg_ref->{json} or croak 'Field json is empty';

	# Call the generic prepare_request subroutine to make a big part of the job.
	$curl = _prepare_request({username => $username, password => $password,
		url => $url});

	# We update the success_code to the expected one.
	$curl->{success_code} = $CREATED;

	$options = [$CONTENT_TYPE];

	# There are a lot of Curl option. A good way to know which to use is to test
	# your request with the bash curl with the option --libcurl and with a file.
	# This option will write a C file corresponding to the request.

	# Option to tell to Curl to send a POST request.
	if(($curl_ret = $curl->{curl}->setopt(CURLOPT_POST, 1)) != CURLE_OK){
		die $curl->{curl}->strerror($curl_ret);
	}

	# Option to add HTTP options. The options must be given as a reference to an
	# array.
	if(($curl_ret = $curl->{curl}->setopt(CURLOPT_HTTPHEADER, $options))
		!= CURLE_OK){
		die $curl->{curl}->strerror($curl_ret);
	}

	# Option to add data to the POST request.
	if(($curl_ret = $curl->{curl}->setopt(CURLOPT_POSTFIELDS, $json))
		!= CURLE_OK){
		die $curl->{curl}->strerror($curl_ret);
	}

	# Option to indicate the size of the added data.
	if(($curl_ret = $curl->{curl}->setopt(CURLOPT_POSTFIELDSIZE_LARGE, length $json))
		!= CURLE_OK){
		die $curl->{curl}->strerror($curl_ret);
	}

	return $curl;
}

# Prepare the Curl object to make a DELETE request.
#
# This subroutine is specific and should be used only for DELETE request.
# At the end of it the Curl object returned can be used directly to perform a
# DELETE request.
#
# There is one parameter which is a reference to an hash which contains fields.
# Those field will be used as parameter and will be described below.
#
# @param $arg_ref A reference to Perl hash.
# @param username The username used to connect to GRID'5000.
# @param password The password of the corresponding username.
# @param url The url to query.
# @return A reference to a Perl hash which contains among other a reference to a
# curl object.
sub _prepare_DELETE_request{
	my $arg_ref;

	my $username;
	my $password;
	my $url;

	my $curl;
	my $curl_ret;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	# Get the parameters.
	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$url = $arg_ref->{url} or croak 'Field url is empty';

	# Call the generic prepare_request subroutine to make a big part of the job.
	$curl = _prepare_request({username => $username, password => $password,
		url => $url});

	# We update the success_code to the expected one.
	$curl->{success_code} = $ACCEPTED;

	# Option to tell to Curl to send a DELETE request.
	# ALERT I found a strange behavior with the option CURLOPT_CUSTOMREQUEST.
	# I used Readonly on a simple quote (') DELETE variable and the
	# CURLOPT_CUSTOMREQUEST seems to fail and leads to a simple GET request.
	# With a colleague we investigated a bit and he found that adding double quote
	# around $DELETE_STRING seems to work.
	# I then replace the simple quote by double quote (") around the value. The
	# request now really does a DELETE.
	# I would like to thank my colleague for his help.
	if(($curl_ret = $curl->{curl}->setopt(CURLOPT_CUSTOMREQUEST, "$DELETE_STRING"))
		!= CURLE_OK){
		die $curl->{curl}->strerror($curl_ret);
	}

	# Option to tell to Curll to not send a GET request.
	if(($curl_ret = $curl->{curl}->setopt(CURLOPT_HTTPGET, 0)) != CURLE_OK){
		die $curl->{curl}->strerror($curl_ret);
	}

	return $curl;
}

# Really perform the request.
#
# This subroutine will perform a Curl request withouth knowing the type of the
# request.
# If the request failed information will be provided to help debuging.
#
# @param $curl A reference to a Perl hash which contains among others field a
# reference to Curl object. This Curl object should have been initialized by a
# prepare_.*_request.
# @return A reference to a Perl hash obtained by decoding the JSON got after
# the request. NOTE This hash reference is empty in the case of a DELETE
# request.
# WARNING If the request failed (i.e. returned code different from
# $success_code) then warn messages are printed and the subroutine returns undef
sub _do_request{
	my $arg_ref;

	my $curl;
	my $header;
	my $json;
	my $url;
	my $success_code;

	my $ret_code;
	my $response_code;

	my $ret;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$curl = $arg_ref->{curl} or croak 'Field curl is empty';
	$success_code = $arg_ref->{success_code}
		or croak 'Field success_code is empty';
	$url = $arg_ref->{url} or croak 'Field url is empty';

	# We do not want to get the actual value of json and header since they are
	# likely to be empty.
	# So we get a reference on those variable we will then be able to dereference
	# them to get the content after the curl request was performed.
	$json = \$arg_ref->{json} or croak 'Field json is empty';
	$header = \$arg_ref->{header} or croak 'Field header is empty';

	$ret_code = $curl->perform();

	if($ret_code == 0){
		$response_code = $curl->getinfo(CURLINFO_HTTP_CODE);

		if($response_code != $success_code){
			warn "Response code must be $success_code and it is $response_code for ",
				"$url\nHeader is:\n${$header}\nAnd data are:\n${$json}";

			return undef;
		}
	}else{
		warn "Request failed: $ret_code\n." . $curl->errbuf();

		return undef;
	}

	# If we give an empty string to decode_json it will die.
	# This test is here to avoid to die.
	if(not length ${$json}){
		return {};
	}

	return decode_json(${$json});
}

sub curl_request{
	my $arg_ref;

	my $request_type;
	my $username;
	my $password;
	my $url;

	my $curl;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$request_type = $arg_ref->{request_type}
		or croak 'Field request_type is empty';
	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$url = $arg_ref->{url} or croak 'Field url is empty';

	# This snippet acts a bit like a factory.
	if($request_type == $POST){
		my $json;

		$json = $arg_ref->{json} or croak 'Field json is empty';

		$curl = _prepare_POST_request({username => $username, password => $password,
			url => $url, json => $json});
	}elsif($request_type == $DELETE){
		$curl = _prepare_DELETE_request({username => $username,
			password => $password, url => $url});
	}elsif($request_type == $GET){
		$curl = _prepare_request({username => $username, password => $password,
			url => $url});
	}else{
		croak "request_type is not an accepted value ($request_type)";
	}

	return _do_request($curl);
}

1;

__END__

=pod

=head1 NAME

G5KMinPerlAPI::Curlez - Perl module to do GET, POST and DELETE request easily.

=head1 SYNOPSIS

	use G5KMinPerlAPI::Curlez qw($GET $POST $DELETE curl_request);

	$json = curl_request({request_type => $GET, username => $username,
		password => $password, url => $url});

	print %{$json};

	$json = curl_request({request_type => $POST, username => $username,
		password => $password, url => $url, json => $json});

	print %{$json};

	$json = curl_request({request_type => $DELETE, username => $username,
		password => $password, url => $url});

	print %{$json};

=head1 DESCRIPTION

G5KMinPerlAPI::Curlez is a Perl module to perform GET, POST and DELETE request
easily. This module is maybe B<< too much specific >> (its first intent is to be
used by the other module of G5KMinPerlAPI).

=head1 GLOBAL VARIABLES

This modules defines 3 global read only variables which indicates the type of
the request. There should be used by the C<< curl_request >> subroutine:

=over 4

=item C<< $GET >>

Indicates that we want to perform a GET request.

=item C<< $POST >>

Indicates that we want to perform a POST request.

=item C<< $DELETE >>

Indicates that we want to perform a DELETE request.

=back

=head1 SUBROUTINE

This module defines one exposed subroutine:

C<< curl_request $HASH_REF >>

This method will perform a Curl request according to the given arguments. The
hash reference B<< must >> contain the following fields:

=over 4

=item C<< request_type >>

This field will contain one of the global variables explained above: $GET, $POST
or $DELETE.

=item C<< username >>

This field will contain the username required by the request. This is a lack of
genericity but since this module is thought to be used on GRID'5000 we need a
username to connect the network.

=item C<< password >>

The password corresponding to the username.

=item C<< url >>

The URL to request.

=item C<< json >>

This field is mandatory only for B<< POST >> request. It will contain a JSON
string which contains arguments passed to the request.

=back

=head1 DIAGNOSTICS

If you forgot to provide the hash reference needed as argument the subroutine
will croak on you.

It will also croak on you if you forgot a mandatory field.

More severe the curl_request can die if the effective curl_request failed in an
inner subroutine. If it effectively died it will print the header and the json
of the response which led to death.

=head1 DEPENDENCIES

This module uses the following Perl module:

=over 4

=item L<< Carp >>

This module is needed because G5KMinPerlAPI::Curlez croak on the user if she/he
gave a bad number of parameters to the subroutines.

=item L<< Readonly >>

G5KMinPerlAPI::Curlez defines several constant. We use this module to define
them.

=item L<< Scalar::Util >>

This module is used to check the type of the reference. It is so mainly used to
strengthen the code.

=item L<< WWW::Curl::Easy >>

G5KMinPerlAPI::Curlez is basically an easy wrapper to this module.

=item L<< JSON >>

The answer of the curl request are given under JSON form. We need this module to
decode them.

=item L<< Exporter >>

G5KMinPerlAPI::Curlez of course exports variables and subroutine. It requires
L<< Exporter >>to do so.

=item L<< base >>

L<< base >> and L<< Exporter >> are used together to exportation.

=back

=head1 INCOMPATIBILITIES

A problem was met with WWW::Curl::Easy and Readonly.
Apprently the option CURLOPT_CUSTOMREQUEST does not like Readonly simply quoted
variable. This variable needs to be enclosed by double quotes to perfectly work.

I would like to thank one of my colleague for the help in the investigation.

=head1 BUGS AND LIMITATIONS

The big limitations of this module is its lack of genericity about the requests.

I do not sincerely think that this module can be imported and used as is for
others request than that of GRID'5000.

=head1 AUTHOR

Francis Laniel (francis.laniel@lip6.fr)

=head1 LICENSE AND COPYRIGHT

Copyright (c) Francis Laniel 2018

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This module is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut