# Copyright (c) 2018 Francis Laniel <francis.laniel@lip6.fr>
#
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
package G5KMinPerlAPI::Common;
use strict;
use warnings;
use lib q{..};
use G5KMinPerlAPI::Curlez qw($GET curl_request);
use Carp;
use Readonly;
use Scalar::Util qw(reftype);
require Exporter;
use base qw(Exporter);

our @EXPORT_OK = qw(get_sites get_clusters get_nodes get_node_characteristics
	get_site_status get_job_characteristics get_deploy_characteristics
	get_node_power_consumption);

# The API URL used for get the sites.
Readonly our $SITES_URL => 'https://api.grid5000.fr/stable/sites/';

# This API URL permits to obtain the status of a specific site.
Readonly our $SITES_STATUS_URL
	=> 'https://api.grid5000.fr/stable/sites/%s/status/';

# The JSON field which contains an array of sites/clusters/nodes.
Readonly our $ITEMS => 'items';

# The JSON field which contains an unique identifier for a sites/clusters/nodes.
Readonly our $UID => 'uid';

# The API URL used for obtain the clusters.
Readonly our $CLUSTERS_URL
	=> 'https://api.grid5000.fr/stable/sites/%s/clusters/';

# The API URL used for gather information about nodes.
Readonly our $NODES_URL
	=> 'https://api.grid5000.fr/stable/sites/%s/clusters/%s/nodes/';

# The API URL used to get characteristics of a given node.
Readonly our $NODE_URL
	=> 'https://api.grid5000.fr/stable/sites/%s/clusters/%s/nodes/%s/';

# The API URL used to get characteristics of a given job.
Readonly our $JOB_URL => 'https://api.grid5000.fr/stable/sites/%s/jobs/%s/';

# The API URL used to get information of the given deployment
Readonly our $DEPLOY_URL
	=> 'https://api.grid5000.fr/stable/sites/%s/deployments/%s';

# The API URL used to gather power consumption of the given node for the given
# time windows.
Readonly our $POWER_URL =>
	'https://api.grid5000.fr/stable/sites/%s/metrics/power/timeseries?only=%s&from=%d&to=%d';

sub get_sites{
	my $arg_ref;

	my $username;
	my $password;

	my $curl;
	my $json;

	my @sites;

	$arg_ref = shift @_
		or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';

	$json = curl_request({request_type => $GET, username => $username,
		password => $password, url => $SITES_URL});

	# If the curl request ends badly the subroutine will return undef.
	# We do not want to dereference undef...
	if(not defined $json){
		return \@sites;
	}

	# Let's enter the 4th dimension !
	# $json is a reference to an hash, we dereference it and then access to the
	# field $ITEMS which is a reference to an array.
	# We then derefence it and we obtain a reference to an array. We just need
	# to use @{} to go through it ! (see perldoc perlreftut to understand the
	# use of references).
	# About the API, a GET on $SITE_URL gives us a JSON object which is
	# basically an array of hash. In each hash we want to get the name of the
	# side which is known as the $UID.
	foreach my $it (@{$json->{$ITEMS}}){
		push @sites, $it->{$UID};
	}

	# WARNING Since Rennes is currently unavailable we remove it from the returned
	# site because nothing can be launched on it.
	@sites = grep {!/rennes/} @sites;

	return \@sites;
}

sub get_site_status{
	my $arg_ref;

	my $username;
	my $password;
	my $site;

	my $url;
	my $curl;

	$arg_ref = shift @_
		or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';

	# Inject the site name into the $SITES_STATUS_URL.
	$url = sprintf $SITES_STATUS_URL, $site;

	return curl_request({request_type => $GET, username => $username,
		password => $password, url => $url});
}

sub get_clusters{
	my $arg_ref;

	my $username;
	my $password;
	my $site;

	my $url;
	my $curl;
	my $json;

	my @clusters;

	$arg_ref = shift @_
		or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';

	# Inject the site name into the $CLUSTERS_URL to get the cluster of site given
	# as parameter.
	$url = sprintf $CLUSTERS_URL, $site;

	$json = curl_request({request_type => $GET, username => $username,
		password => $password, url => $url});

	if(defined $json){
		foreach my $it (@{$json->{$ITEMS}}){
			push @clusters, $it->{$UID};
		}
	}

	return \@clusters;
}

sub get_nodes{
	my $arg_ref;

	my $username;
	my $password;
	my $site;
	my $cluster;

	my $url;
	my $curl;
	my $json;

	my @nodes;

	$arg_ref = shift @_
		or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$cluster = $arg_ref->{cluster} or croak 'Field cluster is empty';

	# Inject the site name and the cluster name into the $NODES_URL to get the
	# nodes of site and cluster given as parameter.
	$url = sprintf $NODES_URL, $site, $cluster;

	$json = curl_request({request_type => $GET, username => $username,
		password => $password, url => $url});

	if(defined $json){
		foreach my $it (@{$json->{$ITEMS}}){
			push @nodes, $it->{$UID};
		}
	}

	return \@nodes;
}

sub get_node_characteristics{
	my $arg_ref;

	my $username;
	my $password;

	my $site;
	my $cluster;
	my $node;

	my $url;

	$arg_ref = shift @_
		or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$cluster = $arg_ref->{cluster} or croak 'Field cluster is empty';
	$node = $arg_ref->{node} or croak 'Field node is empty';

	# Inject the site, cluster an node into the $NODE_URL to get the
	# characteristics of the node given as parameter.
	$url = sprintf $NODE_URL, $site, $cluster, $node;

	return curl_request({request_type => $GET, username => $username,
		password => $password, url => $url});
}

sub get_job_characteristics{
	my $arg_ref;

	my $username;
	my $password;

	my $site;
	my $job_id;

	my $url;

	$arg_ref = shift @_
		or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$job_id = $arg_ref->{job_id} or croak 'Field job_id is empty';

	# Inject the site and job id into the $JOB_URL to get the characteristics of
	# the site given as parameter.
	$url = sprintf $JOB_URL, $site, $job_id;

	return curl_request({request_type => $GET, username => $username,
		password => $password, url => $url});
}

sub get_deploy_characteristics{
	my $arg_ref;

	my $username;
	my $password;

	my $site;
	my $deploy_id;

	my $url;

	$arg_ref = shift @_
		or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$deploy_id = $arg_ref->{deploy_id} or croak 'Field deploy_id is empty';

	# Inject the site and job id into the $JOB_URL to get the characteristics of
	# the site given as parameter.
	$url = sprintf $DEPLOY_URL, $site, $deploy_id;

	return curl_request({request_type => $GET, username => $username,
		password => $password, url => $url});
}

sub get_node_power_consumption{
	my $arg_ref;

	my $username;
	my $password;

	my $site;
	my $node;

	my $from;
	my $to;

	my $url;

	$arg_ref = shift @_
		or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$node = $arg_ref->{node} or croak 'Field job_id is empty';
	$from = $arg_ref->{from} or croak 'Field from is empty';
	$to = $arg_ref->{to} or croak 'Field to is empty';

	# Inject the site, node, from date en to date into the $POWER_URL to get the
	# characteristics of the site given as parameter.
	$url = sprintf $POWER_URL, $site, $node, $from, $to;

	return curl_request({request_type => $GET, username => $username,
		password => $password, url => $url});
}

1;

__END__

=pod

=head1 NAME

G5KMinPerlAPI::Common - Perl module to get information about the GRID'5000
network.

=head1 SYNOPSIS

	use G5KMinPerlAPI::Common;

	$sites = get_sites($username, $password);
	$clusters = get_clusters($username, $password, @$sites[0]);
	$nodes = get_nodes($username, $password, $sites[0], @$clusters[0]);

	print "Sites of GRID'5000 are:\n";
	foreach my $it (@$sites){
		print "* $it\n";
	}

	print "Clusters of ", @$sites[0], " are:\n";
	foreach my $it (@$clusters){
		print "* $it\n";
	}

	print "Nodes of ", @$sites[0], ".", @$clusters[0], " are:\n";
	foreach my $it (@$nodes){
		print "* $it\n";
	}

	print %{get_node_characteristics()}, "\n";
	print %{get_job_characteristics()}, "\n";

=head1 DESCRIPTION

G5KMinPerlAPI::Common is a Perl module which permits user of the GRID'5000
network to get information about the network.
It is possible to :

=over

=item * get the sites names which are parts of the network,

=item * get the clusters names which compose a site,

=item * get the nodes (i.e. the computers) which belong to a cluster,

=item * get the status of a site (e.g. the reservation states for each nodes),

=item * get the characteristics of a node,

=item * get the characteristics of a job,

=item * and get the characteristics of a deployment.

=back

=head1 SUBROUTINES

=over 4

=item C<< get_sites HASH_REF >>

This functions returns all the site which are parts of the Grid'5000 network.

The argument is an hash reference which must contain the two following fields:

=over 4

=item username

The username used to connect to the GRID'5000 network.

=item password

The password associated to the above username.

=back

A site is a city, for example B<< grenoble >>.
It returns a reference to an array containing all the sites.

If an inner curl request failed the returned array will be empty.

=item C<< get_site_status HASH_REF >>

This functions returns the status of the site given as parameter under a decoded
JSON form (i.e a Perl hash).

If an inner curl request failed the subroutine will return undef instead of a
Perl hash reference.

The argument is an hash reference which must contain the three following fields:

=over 4

=item username

The username used to connect to the GRID'5000 network.

=item password

The password associated to the above username.

=item site

The name of the site we want to obtain status.

=back

This hash can be used by the subroutines of G5KMinPerlAPI::Capable to get
informations of the nodes of the site (e.g if a specific site is reserved).

=item C<< get_clusters HASH_REF >>

This functions returns all the cluster which belong to a site. A cluster is a
group of machines, for example B<< graphite >> in the B<< nancy >> site.

The argument is an hash reference which must contain the three following fields:

=over 4

=item username

The username used to connect to the GRID'5000 network.

=item password

The password associated to the above username.

=item site

We want to obtain the clusters which are parts of this site.

=back

It returns a reference to an array containing all the clusters of the site given
as third field.

If an inner curl request failed the returned array will be empty.

=item C<< get_nodes HASH_REF >>

This functions returns all the nodes which are inside a cluster. A node is a
machine.

The argument is an hash reference which must contain the four following fields:

=over 4

=item username

The username used to connect to the GRID'5000 network.

=item password

The password associated to the above username.

=item site

A node is part of a site.

=item cluster

We want to obtain the nodes belonging to the cluster given as field which is
part of the site given above.

=back

For example B<< paravance-66 >> which is part of the B<< paravance >>
cluster in B<< rennes >>.
It returns a reference to an array containing all the nodes of the site and
cluster given as last fields.

If an inner curl request failed the returned array will be empty.

=item C<< get_node_characteristics HASH_REF >>

This functions returns all the characteristics of the node given as parameter
under a decoded JSON form (i.e a Perl hash).

If an inner curl request failed the subroutine will return undef instead of a
Perl hash reference.

The argument is an hash reference which must contain the five following fields:

=over 4

=item username

The username used to connect to the GRID'5000 network.

=item password

The password associated to the above username.

=item site

A node is part of a site.

=item cluster

A node is part of a cluster which belongs to a site.

=item node

The name of the node on which we want to get the characteristics.

=back

This hash can be used by the subroutines of G5KMinPerlAPI::Capable to get
informations of the node.

=item C<< get_job_characteristics HASH_REF >>

This functions returns all the characteristics of the job given as parameter
under a decoded JSON form (i.e a Perl hash).

If an inner curl request failed the subroutine will return undef instead of a
Perl hash reference.

The argument is an hash reference which must contain the four following fields:

=over 4

=item username

The username used to connect to the GRID'5000 network.

=item password

The password associated to the above username.

=item site

A job belongs to a site.

=item job_id

The unique identifier of the job whom we want to get characteristics.

=back

This hash can be used by the subroutine G5KMinPerlAPI::Capable::is_running to
check if the job is running.

=item C<<  get_deploy_characteristics HASH_REF >>

This subroutine returns all the characteristics of the deployment given as
parameter under a decoded JSON form (i.e a Perl hash).

The argument is an hash reference which must contain the four following fields:

=over 4

=item username

The username used to connect to the GRID'5000 network.

=item password

The password associated to the above username.

=item site

A deployment belongs to a site.

=item deploy_id

The unique identifier of the deployment whom we want to get characteristics.

=back

This hash can be used by the subroutine G5KMinPerlAPI::Capable::is_deployed to
check if the deployment is ready to be used.

=item C<< get_node_power_consumption HASH_REF >>

This subroutine returns the power consumption values and timestamps for the
given node and timewindow. Those data are returned under a decoded JSON string
(i.e. a reference to a Perl hash).

If an inner curl request failed the subroutine will return undef instead of a
Perl hash reference.

The argument is an hash reference which must contain the six following fields:

=over 4

=item username

The username used to connect to the GRID'5000 network.

=item password

The password associated to the above username.

=item site

A node belongs to a site.

=item node

The node we want to monitor power consumption.

=item from

This date in seconds indicates the beginning of the time window we want to
monitor.

=item to

This date in seconds indicates the end of the time window we want to
monitor.

=back

The returned hash will mainly contain two arrays of the same dimension:

=over 4

=item * the first one contains timestamps,

=item * and the second holds power values (in Watt) for the corresponding
timestamp.

=back

For example, values[n] is the power consumed at timestamp[n]. There is one value
for each second.

=back

=head1 DIAGNOSTICS

If you forgot to provide the hash reference needed as argument the subroutines
will croak on you.

They will also croak on you if you forgot a mandatory field.

Due to an inner call to the exported subroutine of G5KMinPerlAPI::Curlez the
subroutines get_sites, get_clusters and get_nodes can returned empty array.

Due to the same inner call the subroutines get_site_status,
get_deploy_characteristics, get_job_characteristics, get_node_characteristics
and get_node_power_consumption can return undef.

=head1 DEPENDENCIES

This module uses the following Perl module:

=over 4

=item L<< Carp >>

This module is needed because G5KMinPerlAPI::Common croak on the user if she/he
gave a bad number of parameters to the subroutines.

=item L<< Readonly >>

G5KMinPerlAPI::Common defines several constant. We use this module to define
them.

=item L<< Scalar::Util >>

This module is used to check the type of the reference. It is so mainly used to
strengthen the code.

=item L<< G5KMinPerlAPI::Curlez >>

G5KMinPerlAPI::Common uses this module to query the GRID'5OOO API.

=item L<< Exporter >>

G5KMinPerlAPI::Common of course exports variables and subroutine. It requires
L<< Exporter >>to do so.

=item L<< base >>

L<< base >> and L<< Exporter >> are used together to exportation.

=back

=head1 INCOMPATIBILITIES

Until now there is no known incompatibility.

=head1 AUTHOR

Francis Laniel (francis.laniel@lip6.fr)

=head1 LICENSE AND COPYRIGHT

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This module is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut