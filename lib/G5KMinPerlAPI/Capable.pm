# Copyright (c) 2018 Francis Laniel <francis.laniel@lip6.fr>
#
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
package G5KMinPerlAPI::Capable;
use strict;
use warnings;
use Carp;
use Readonly;
use Scalar::Util qw(reftype);
require Exporter;
use base qw(Exporter);

our @EXPORT_OK = qw(is_API_and_not_shared_power_capable is_deploy_capable
	is_sold_by_the_devil is_available_and_not_reserved is_running is_deployed);

# The JSON field which contains an hash of the differents sensors that the node
# had.
Readonly our $SENSORS => 'sensors';

# The JSON field which contains an hash of the power characteristics of the
# node.
Readonly our $POWER => 'power';

# A JSON field which contains true if the power can be measured on this node.
Readonly our $AVAILABLE => 'available';

# If this JSON field is true, then the power obtained through the API for this
# node is the power of this node and not the sum of the power of several nodes.
Readonly our $PER_OUTLETS => 'per_outlets';

# A JSON field which contains an hash of the manners to get the power of the
# node.
Readonly our $VIA => 'via';

# A JSON field which contains an hash of information to obtain the power through
# the API.
Readonly our $API => 'api';

# A JSON field which contains an hash of all the type of jobs the node accepts.
Readonly our $JOBS => 'supported_job_types';

# A JSON field which contains true if the node accepts 'deploy' jobs.
Readonly our $DEPLOY => 'deploy';

# A JSON field which contains an hash of information about the CPU of the node.
Readonly our $PROCESSOR => 'processor';

# A JSON field which contains a string which is the name of CPU seller of the
# node.
Readonly our $VENDOR => 'vendor';

# A string which contains the name of the devil itself. It must not be printed !
Readonly our $DEVIL => 'Intel';

# The URL of GRID5000 network it is used to reform the complete node name.
Readonly our $GRID5000_URL => 'grid5000.fr';

# A JSON field which contains an hash whom keys are the node names.
Readonly our $NODES => 'nodes';

# A JSON field which indicates the state of the hardware of the node.
Readonly our $HARD => 'hard';

# A string which is one of the value of the $HARD field. This is the value that
# interest us.
Readonly our $ALIVE => 'alive';

# A JSON field which indicates the state of the software of the node.
Readonly our $SOFT => 'soft';

# A string which is one of the value of $SOFT field. This value interest us.
Readonly our $FREE => 'free';

# A string which is one of the value of $SOFT field. This value interest us
# since a besteffort job will be canceled by a classic job.
Readonly our $BESTEFFORT => 'busy_besteffort';

# A JSON field which contains the name of the job queue where the reservation
# was done.
Readonly our $QUEUE => 'queue';

# A string which is one of the of $QUEUE field. This value interest us since a
# besteffort job will be canceled by a classic job.
Readonly our $QUEUE_BESTEFFORT => 'besteffort';

# A JSON field which contains the date in second of the beginning ot the
# reservation.
Readonly our $SCHEDULED_AT => 'scheduled_at';

# A JSON field which contains an array of reservations.
Readonly our $RESERVATIONS => 'reservations';

# A JSON field which contains the state of the job.
Readonly our $STATE => 'state';

# A string which indicates that the job is running.
Readonly our $RUNNING => 'running';

# A JSON field which contains the state of the deployment.
Readonly our $STATUS => 'status';

# A string which indicates that the deployment is terminated.
Readonly our $TERMINATED => 'terminated';

sub is_API_and_not_shared_power_capable{
	my $json;
	my $power_json;

	$json = shift @_ or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $json or reftype $json ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	# The kind of JSON we want to get looks like this :
	# "sensors": {
	# 	"power": {
	# 		"available": true,
	# 		"per_outlets": true,
	# 		"via": {
	# 			"api": {
	# 				"metric": "power"
	# 			}
	# 		}
	# 	}
	# }
	# We test if the "sensors" label contains a reference to an hash.
	if(defined $json->{$SENSORS}
		&& reftype($json = $json->{$SENSORS}) ne reftype {}){
		return 0;
	}

	# We test if the "power" label into the sensors hash contains another
	# reference to an hash.
	if(defined $json->{$POWER}
		&& reftype($power_json = $json->{$POWER}) ne reftype {}){
		return 0;
	}

	# We test the value of the labels "available" and "per_outlets".
	unless($power_json->{$AVAILABLE} && $power_json->{$PER_OUTLETS}){
		return 0;
	}

	# We test if the label "api" into the hash "via" (which is inside the hash
	# power) holds a reference to an hash.
	if(reftype $power_json->{$VIA}->{$API} eq reftype {}){
		return 1;
	}

	return 0;
}

sub is_deploy_capable{
	my $json;

	$json = shift @_ or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $json or reftype $json ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	# The kind of JSON we want to analyse looks like:
	# "supported_job_types": {
	# 	"besteffort": true,
	# 	"deploy": true,
	# 	"max_walltime": 0,
	# 	"queues": [
	# 		"default",
	# 		"admin"
	# 	],
	# 	"virtual": false
	# }
	# We test if the "supported_job_types" label contains a reference to an hash.
	if(reftype($json = $json->{$JOBS}) eq reftype {}){
		return $json->{$DEPLOY};
	}

	return 0;
}

sub is_sold_by_the_devil{
	my $json;

	$json = shift @_ or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $json or reftype $json ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	# The kind of JSON we want to analyse looks like:
	# "processor": {
	# 	"instruction_set": "x86-64",
	# 	"microarchitecture": "Lynnfield",
	# 	"model": "Intel Xeon",
	# 	"other_description": "Intel(R) Xeon(R) CPU           X3440  @ 2.53GHz",
	# 	"vendor": "Intel",
	# 	"version": "X3440"
	# }
	# We test if the "processor" label contains a reference to an hash.
	if(reftype($json = $json->{$PROCESSOR}) eq reftype {}){
		return $json->{$VENDOR} eq $DEVIL;
	}

	return 0;
}

sub is_available_and_not_reserved{
	my $arg_ref;

	my $json;
	my $site;
	my $node;
	my $job_time;

	my $node_string;

	$arg_ref = shift @_
		or croak 'Must provide 1 argument (a reference to an hash)';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$json = $arg_ref->{json} or croak 'Field json is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$node = $arg_ref->{node} or croak 'Field node is empty';
	$job_time = $arg_ref->{job_time};

	if(not defined reftype $json or reftype $json ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	# The JSON that interests us looks like this :
	# "nodes": {
	# 	"grisou-8.nancy.grid5000.fr": {
	# 		"hard": "alive",
	# 		"soft": "busy_besteffort",
	# 		"reservations": [
	#
	# 		],
	# 		"comment" : "OK"
	# 	},
	# 	etc.
	# 	"talc-70.nancy.grid5000.fr": {
	# 		"hard": "alive",
	# 		"soft": "free",
	# 		"reservations": [
	#
	# 		],
	# 		"comment": "OK"
	# 	}
	# }
	# We test if the "nodes" label contains a reference to an hash.
	if(reftype $json->{$NODES} ne reftype {}){
		return 0;
	}

	# We reconstruct the complete node name. The complete node name is formed of:
	# * the short node name (e.g paravance-66),
	# * the site where the node is (e.g. rennes),
	# * and the url of the network (grid5000.fr)
	# For example: paravance-66.rennes.grid5000.fr
	$node_string = "$node.$site.$GRID5000_URL";

	# We test if the value is defined.
	unless(defined $json->{$NODES}->{$node_string}){
		carp "Maybe the node '$node' is not part of the site '$site'?";

		return 0;
	}

	# We test if the complete node name label contains a reference to an hash.
	if(reftype $json->{$NODES}->{$node_string} ne reftype {}){
		return 0;
	}

	# If the hardware is not marked as alive then the node is not available.
	if($json->{$NODES}->{$node_string}->{$HARD} ne $ALIVE){
		return 0;
	}

	# If the software is not marked as free then the node is not available.
	# "Free" as in "free speech" not as in "free beer".
	# besteffort jobs can be canceled by classical job type so we also test this
	# value.
	if($json->{$NODES}->{$node_string}->{$SOFT} ne $FREE
		and $json->{$NODES}->{$node_string}->{$SOFT} ne $BESTEFFORT){
		return 0;
	}

	# $RESERVATIONS field must be an array.
	if(reftype $json->{$NODES}->{$node_string}->{$RESERVATIONS} ne reftype []){
		return 0;
	}

	# If its array is empty then the node is not reserved at all.
	if(scalar @{$json->{$NODES}->{$node_string}->{$RESERVATIONS}} == 0){
		return 1;
	}

	# We will analyze each reservations.
	# If we arrive here normally we deal only with upcoming reservations.
	foreach my $it (@{$json->{$NODES}->{$node_string}->{$RESERVATIONS}}){
		# If the reservation is of type besteffort we do not care of its time since
		# we can kill it if we submit a job during it. I agree it is a bit devilish
		# but sometimes you need to be...
		if($it->{$QUEUE} ne $QUEUE_BESTEFFORT){
			# If the reservation is scheduled during our job then our job will be
			# killed. So we mark the node as not available.
			if($it->{$SCHEDULED_AT} < time + $job_time){
				return 0;
			}
		}
	}

	return 1;
}

sub is_running{
	my $json;

	$json = shift @_ or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $json or reftype $json ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	# We want to analyze this type of JSON:
	# {
	# 	"uid": 400989,
	# 	"user_uid": "crohr",
	# 	"user": "crohr",
	# 	"walltime": 7200,
	# 	"queue": "default",
	# 	"state": "running",
	# }
	# We compare the value of the state field to running.
	return $json->{$STATE} eq $RUNNING;
}

sub is_deployed{
	my $json;

	$json = shift @_ or croak 'Must provide 1 argument (a reference to an hash).';

	if(not defined reftype $json or reftype $json ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	# We want to analyze this type of JSON:
	# {
	# 	"created_at": 1325776788,
	# 	"environment": "wheezy-x64-base",
	# 	"key": "https://api.grid5000.fr/3.0/sites/rennes/files/crohr-key-d496411f889ff24890248359840358ab8107211b",
	# 	"nodes": [
	# 		"parapluie-8.rennes.grid5000.fr"
	# 	],
	# 	"site_uid": "rennes",
	# 	"status": "processing",
	# 	"uid": "eeb14c15813be91c126e78e30e42e0812874d3b3",
	# 	"updated_at": 1325776788,
	# 	"user_uid": "crohr",
	# }
	# We compare the value of the status field to terminated.
	return $json->{$STATUS} eq $TERMINATED;
}

1;

__END__

=pod

=head1 NAME

G5KMinPerlAPI::Capable - Perl module to get characteristics of the nodes
belonging to the GRID'5000 network.

=head1 SYNOPSIS

	use G5KMinPerlAPI::Capable;

	print is_API_and_not_shared_power_capable($json) ? "Node is power capable.\n"
		: "Node is not power capable.\n";

	print is_deploy_capable($json) ? "Node is deploy capable.\n"
		: "Node is not power capable.\n";

	print is_sold_by_the_devil($json) ? "Node is sold by the devil.\n"
		: "Hooray!\n";

	# Change the value of the JSON to be a "site" JSON.
	$json = ...;

	print is_available_and_not_reserved($json, $site, $node)
		? "Node is available.\n" : "Node is not available.\n";

	# Change the value of the JSON to be a "job" JSON.
	$json = ...;

	print is_running($json) ? "Job is running.\n" : "Job is not running.";

=head1 DESCRIPTION

G5K::Capable is a perl module which permits user of the GRID'5000 network to get
information about nodes of the network.
It is possible to check if:

=over

=item * the power consumption of a node can be measurable,

=item * the node accepts "deploy" jobs,

=item * the node CPU is sold by a specific processor manufacturer,

=item * the node is available and not reserved by someone else

=item * and a job is running

=back

=head1 SUBROUTINES

=over 4

=item C<< is_API_and_not_shared_power_capable JSON >>

This subroutine returns 1 if the node given as parameter (under a reference to
Perl hash obtained by decoding JSON) can have its power measured independently
and through the API.

=item C<< is_deploy_capable JSON >>

This subroutine returns 1 if the node given as parameter (under a reference to
Perl hash obtained by decoding JSON) accepts "deploy" jobs.

=item C<< is_sold_by_the_devil JSON >>

This subroutine returns 1 if the node given as parameter (under a reference to
Perl hash obtained by decoding JSON) has a CPU sold by Intel.

=item C<< is_available_and_not_reserved HASH_REF >>

This subroutine takes a reference to an hash which must contains the following
fields:

=over 4

=item json

A reference to a Perl hash obtained through JSON decoding which represents the
site,

=item site

The site name,

=item node

The node name name. The node name B<< must >> be part of the site.

=item job_time

An indicative duration B<< in second >> of the job the user want to submit. This
parameter is not mandatory and if not given will be equal to 0.

=back

This subroutine returns 1 if the node given as parameter is available and if
there is no reservations wich belongs to [now, now + job_time] time slice.
So the user can use it to launch jobs.

=item C<< is_running JSON >>

This subroutine returns 1 if the job given as parameter (under a reference to a
Perl hash obtained by decoding JSON) is currently running.

=item C<< is_deployed JSON >>

This subroutine returns 1 if the deployment given as parameter (under a
reference to a Perl hash obtained by decoding JSON) is terminated (i.e. ready
to accept ssh connection).

=back

=head1 DIAGNOSTICS

If you provide a bad number of argument to subroutines they will croak on you
indicating the number of arguments required.

is_available_and_not_reserved can carp on you if you provide a node or a site
which are not into the JSON we want to analyze.

All subroutines return 0 to indicate that the function failed and 1 if they
succedeed.

=head1 DEPENDENCIES

This module uses the following Perl module:

=over 4

=item L<< Carp >>

This module is needed because G5KMinPerlAPI::Common croak on the user if she/he
gave a bad number of parameters to the subroutines.

=item L<< Readonly >>

G5KMinPerlAPI::Common defines several constant. We use this module to define
them.

=item L<< Scalar::Util >>

This module it mainly used to check if the JSON given as argument is formated as
expected.

=item L<< Exporter >>

G5KMinPerlAPI::Common of course exports variables and subroutine. It requires
L<< Exporter >>to do so.

=item L<< base >>

L<< base >> and L<< Exporter >> are used together to exportation.

=back

=head1 INCOMPATIBILITIES

Until now there is no known incompatibility.

=head1 AUTHOR

Francis Laniel (francis.laniel@lip6.fr)

=head1 LICENSE AND COPYRIGHT

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This module is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut