# Copyright (c) 2018 Francis Laniel <francis.laniel@lip6.fr>
#
# This file is part of G5KMinPerlAPI.
#
# G5KMinPerlAPI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# G5KMinPerlAPI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with G5KMinPerlAPI.  If not, see <http://www.gnu.org/licenses/>.
package G5KMinPerlAPI::Jobs;
use strict;
use warnings;
use Carp;
use Readonly;
use Scalar::Util qw(reftype);
use lib q{..};
use G5KMinPerlAPI::Curlez qw($POST $DELETE curl_request);
use Cpanel::JSON::XS;
require Exporter;
use base qw(Exporter);

our @EXPORT_OK = qw(reserve_deploy_node delete_job do_deploy);

# A JSON field used to indicate the resources we want to get (e.g. walltime).
Readonly our $RESOURCES => 'resources';

# A JSON field used to launch a command on launch of the job.
# ALERT This field is mandatory and it must have a value, forget it leads to an
# invalid submission.
Readonly our $COMMAND => 'command';

# A JSON field used to indicate the properties we want to get (e.g. specific
# cluster).
Readonly our $PROPERTIES => 'properties';

# A JSON field used to indicated the types of the job we want to submit (e.g.
# deploy).
Readonly our $TYPES => 'types';

# String which permits to add JSON data to a POST request done with Curl.
# You can check curl_POST_request for more information.
Readonly our $CONTENT_TYPE => 'Content-Type: application/json';

# The URL of GRID5000 network, it is used to reform the complete node name.
Readonly our $GRID5000_URL => 'grid5000.fr';

# The API URL used to submit jobs.
Readonly our $JOBS_URL => 'https://api.grid5000.fr/stable/sites/%s/jobs';

# The API URL used to delete a job. The job_id is given as the last string
# format.
Readonly our $JOB_ID_URL => 'https://api.grid5000.fr/3.0/sites/%s/jobs/%s';

# This property is used to choose a specific node;
Readonly our $HOST => 'host';

# This variable will be the value for the key $COMMAND.
# ALERT This field has to have a value. Moreover we use a infinite loop to avoid
# the job to exit before we deploy our environment.
Readonly our $DUMMY_COMMAND => 'while [ true ]; do sleep 5; done';

# The value to indicate that we want to submit a job whom type is deploy.
Readonly our $DEPLOY => ['deploy'];

# A JSON field which contains an array of the site where environment should be
# deployed.
Readonly our $NODES => 'nodes';

# The API URL use to deploy the reserved nodes.
Readonly our $DEPLOYMENTS_URL =>
	'https://api.grid5000.fr/3.0/sites/%s/deployments';

# A JSON field which contains the name of the environment to deploy.
Readonly our $ENVIRONMENT => 'environment';

# A JSON field which contains the public key of $username.
Readonly our $SSH_KEY => 'key';

# Create a JSON string.
#
# This method will not be exported and will stay private. It is called by a lot
# of subroutines of this module.
#
# There is one parameter which is a reference to an hash which contains fields.
# Those field will be used as parameter and will be described below.
#
# @param @_ The array argument must be even sized. Even cells contains the keys
# and odd one the value.
# @return A string obtained by encoding a Perl hash reference to JSON.
sub _create_JSON{
	my $json;

	my $len;

	$len = scalar @_;

	if($len % 2){
		croak 'Size is not even';
	}

	$json = {};

	for(my $i = 0; $i < $len; $i += 2){
		# cell $i is the key and cell $i + 1 is the value of the previous key.
		$json->{$_[$i]} = $_[$i + 1];
	}

	return encode_json($json);
}

sub reserve_deploy_node{
	my $arg_ref;

	my $username;
	my $password;
	my $site;
	my $node;
	my $resources;

	my $node_string;
	my $properties;
	my $url;
	my $json;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$node = $arg_ref->{node} or croak 'Field node is empty';
	$resources = $arg_ref->{ressources} or croak 'Field ressources is empty';

	if(reftype $DEPLOY ne reftype []){
		croak '$DEPLOY must be an array reference and it is ', reftype $DEPLOY;
	}

	# We reconstruct the complete node name. The complete node name is formed of:
	# * the short node name (e.g paravance-66),
	# * the site where the node is (e.g. rennes),
	# * and the url of the network (grid5000.fr)
	# For example: paravance-66.rennes.grid5000.fr
	$node_string = "$node.$site.$GRID5000_URL";

	# ALERT the properties must be given under a SQL form.
	# So every right part of the equal sign must be put between simple quote (').
	$properties = "$HOST = '$node_string'";

	# This call creates a JSON string which is an hash with 4 fields:
	# * the first corresponds to the resources we want (e.g. cpu=24),
	# * the second is a command to launch at the begining of the job,
	# * the third contains the properties of the reservation (e.g. host =
	# 'graphene-77.nancy.grid5000.fr'),
	# * the last is an array of the types of the jobs (e.g.
	# ['besteffort', 'deploy']).
	$json = _create_JSON($RESOURCES, $resources, $COMMAND, $DUMMY_COMMAND,
		$PROPERTIES, $properties, $TYPES, $DEPLOY);

	# Inject the site name into the $JOBS_URL.
	$url = sprintf $JOBS_URL, $site;

	return curl_request({request_type => $POST, username => $username,
		password => $password, url => $url, json => $json});
}

sub delete_job{
	my $arg_ref;

	my $username;
	my $password;
	my $site;
	my $job_id;

	my $url;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$job_id = $arg_ref->{job_id} or croak 'Field job_id is empty';

	# Inject the site and the job_id into the $JOB_ID_URL.
	$url = sprintf $JOB_ID_URL, $site, $job_id;

	return curl_request({request_type => $DELETE, username => $username,
		password => $password, url => $url});
}

sub do_deploy{
	my $arg_ref;

	my $username;
	my $password;
	my $site;
	my $node;
	my $environment;
	my $ssh_key;

	my $node_string;
	my $json;
	my $url;

	$arg_ref = shift @_
		or croak 'Must provide one argument (a reference to an hash).';

	if(not defined reftype $arg_ref or reftype $arg_ref ne reftype {}){
		croak 'The argument must be an hash reference.';
	}

	$username = $arg_ref->{username} or croak 'Field username is empty';
	$password = $arg_ref->{password} or croak 'Field password is empty';
	$site = $arg_ref->{site} or croak 'Field site is empty';
	$node = $arg_ref->{node} or croak 'Field node is empty';
	$environment = $arg_ref->{environment} or croak 'Field environment is empty';
	$ssh_key = $arg_ref->{ssh_key} or croak 'Field ssh_key is empty';

	# We reconstruct the complete node name. The complete node name is formed of:
	# * the short node name (e.g paravance-66),
	# * the site where the node is (e.g. rennes),
	# * and the url of the network (grid5000.fr)
	# For example: paravance-66.rennes.grid5000.fr
	$node_string = "$node.$site.$GRID5000_URL";

	# This call creates a JSON string which is an hash with 3 fields:
	# * the first is an array of the nodes we want to used for the deployment
	# (e.g. ['parapluie-8.rennes.grid5000.fr', 'parapluie-9.rennes.grid5000.fr']),
	# * the second is the name of the environment to deploy
	# (e.g 'debian9-x64-min'),
	# * the last contains you ssh public key.
	$json = _create_JSON($NODES, [$node_string], $ENVIRONMENT, $environment,
		$SSH_KEY, $ssh_key);

	# Inject the site name into the $DEPLOYMENTS_URL.
	$url = sprintf $DEPLOYMENTS_URL, $site;

	return curl_request({request_type => $POST, username => $username,
		password => $password, url => $url, json => $json});
}

1;

__END__

=pod

=head1 NAME

G5KMinPerlAPI::Jobs - Perl module to launch jobs on the GRID'5000 network.

=head1 SYNOPSIS

	use G5KMinPerlAPI::Jobs;

=head1 DESCRIPTION

G5KMinPerlAPI::Jobs is a Perl module which permits user of the GRID'5000
network to launch jobs on the nodes of the network.

=head1 SUBROUTINES

=over 4

=item C<< reserve_deploy_node HASH_REF >>

This subroutine permits to reserve a specific node on a site with given
resources.

The hash reference B<< must >> contain the following field:

=over 4

=item C<< username >>

This field will contain the username required by the request. This is a lack of
genericity but since this module is thought to be used on GRID'5000 we need a
username to connect the network.

=item C<< password >>

The password corresponding to the username.

=item site

A node is part of a site.

=item node

The name of the node on we want to reserve.

=item ressources

The resources are given as a string, for example "cpu=24,walltime=2:0:0"
(you can check the documentation of OAR to get more information).

=back

B<< Pay attention >> if the node does not belong to the site this subroutine
will return undef because an inner request failed by returning a bad
code.

If the same inner request fail for other reasons this subroutine will also
return undef.

=item C<< delete_job HASH_REF >>

This subroutine permits to delete a running job. Beware, it does not test that
the job is currently running.

It returns a reference to an empty hash because DELETE request does not give
back JSON. If an inner request failed it will return undef.

The argument is an hash reference which must contain the four following fields:

=over 4

=item C<< username >>

This field will contain the username required by the request. This is a lack of
genericity but since this module is thought to be used on GRID'5000 we need a
username to connect the network.

=item C<< password >>

The password corresponding to the username.

=item site

A job belongs to a site.

=item job_id

The unique identifier of the job whom we want to get characteristics.

=back

=item C<< do_deploy HASH_REF >>

This subroutine launch the deployment of the environment on the node given as
parameter. The node should have already been reserved before using this
subroutine.

The hash reference B<< must >> contain the following field:

=over 4

=item C<< username >>

This field will contain the username required by the request. This is a lack of
genericity but since this module is thought to be used on GRID'5000 we need a
username to connect the network.

=item C<< password >>

The password corresponding to the username.

=item site

A node is part of a site.

=item node

The node which was reserved using L<< reserve_deploy_node >>.

=item environment

The name of the environment to be deployed (e.g. debian-x64-min).

=item ssh_key

The public ssh key of the user so she/he will be able to ssh on the deployed
node once the deployment finished.

=back

=back

=head1 DIAGNOSTICS

If you forgot to provide the hash reference needed as argument the subroutine
will croak on you.

It will also croak on you if you forgot a mandatory field.

Due to an inner call to the exported subroutine of G5KMinPerlAPI::Curlez the
subroutines can return undef.

=head1 DEPENDENCIES

This module uses the following Perl module:

=over 4

=item L<< Carp >>

This module is needed because G5KMinPerlAPI::Common croak on the user if she/he
gave a bad number of parameters to the subroutines.

=item L<< Readonly >>

G5KMinPerlAPI::Jobs defines several constant. We use this module to define
them.

=item L<< Scalar::Util >>

This module is used to check the type of the reference. It is so mainly used to
strengthen the code.

=item L<< G5KMinPerlAPI::Curlez >>

G5KMinPerlAPI::Jobs uses this module to query the GRID'5OOO API.

=item L<< Cpanel::JSON::XS >>

G5KMinPerlAPI::Jobs needs this module to translate a Perl hash to a JSON string
which will be used by G5KMinPerlAPI::Curlez.

This module was choosed rather than classical JSON because it is possible to use
thread with this one.

=item L<< Exporter >>

G5KMinPerlAPI::Jobs of course exports variables and subroutine. It requires
L<< Exporter >>to do so.

=item L<< base >>

L<< base >> and L<< Exporter >> are used together to exportation.

=back

=head1 INCOMPATIBILITIES

Until now there is no known incompatibility.

=head1 AUTHOR

Francis Laniel (francis.laniel@lip6.fr)

=head1 LICENSE AND COPYRIGHT

Copyright (c) Francis Laniel 2018

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This module is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut